<?php
class M_member extends CI_Model{
	function __construct(){
        parent::__construct();
    }

	function getAllJenisItem(){
		return $this->db->get('tb_jenis_item');
	}

	function cekMember($email,$password){
		return	$q = $this->db->where('email',$email)
					  ->where('password',$password)
					  ->get('tb_member');

	}

	function cekEmail($email){
		return	$q = $this->db->where('email',$email)
					  	  	  ->get('tb_member');

	}

	function registerMember($data){
		return $q = $this->db->insert('tb_member',$data);
	}

	function updateMember($data){
		return $q = $this->db->where('email', $data['email'])
			->update('tb_member',$data);
	}

	function cekChart($id_member){
		return $q = $this->db->join('tb_transaksi_detail','tb_transaksi_detail.id_transaksi=tb_transaksi.id_transaksi','inner')
							 ->where('tb_transaksi.id_member',$id_member)
							 ->where('tb_transaksi.status_transaksi',0)
							 ->get('tb_transaksi');
	}

    function cekPaketChart($id_member){
		return $q = $this->db->join('tb_transaksi_paket_detail','tb_transaksi_paket_detail.id_transaksi_paket=tb_transaksi_paket.id_transaksi_paket','inner')
							 ->where('tb_transaksi_paket.id_member',$id_member)
							 ->where('tb_transaksi_paket.status_transaksi',0)
							 ->get('tb_transaksi_paket');
	}

	function cekOldChart($id_member){
		return $q = $this->db->where('id_member',$id_member)->where('status_transaksi',0)->get('tb_transaksi');
	}

    function cekOldPaketChart($id_member){
		return $q = $this->db->where('id_member',$id_member)->where('status_transaksi',0)->get('tb_transaksi_paket');
	}

	function newCart($cart){
		$this->db->insert('tb_transaksi',$cart);
	}

    function newPaketCart($cart){
		$this->db->insert('tb_transaksi_paket',$cart);
	}

	function getAllPaket(){
		return $q = $this->db->select('nama_upacara, p.*')
			->where('p.status','tidak promo')
			->join('tb_upacara u','u.id_upacara = p.upacara_id', 'inner')
			->get('tb_paket p');
	}

	function getPaketByUpacara($id){
		return $q = $this->db->select('nama_upacara, p.*')
			->join('tb_upacara u','u.id_upacara = p.upacara_id', 'inner')
			->where('p.upacara_id', $id)
			->get('tb_paket p');
	}

	function search($search){
		return $q = $this->db->select('nama_upacara, p.*')
			->join('tb_upacara u','u.id_upacara = p.upacara_id', 'inner')
			->where("nama_paket like '%$search%'")
			->get('tb_paket p');
	}

	function searchItem($search){
		return $q = $this->db->select('i.*')
			->join('tb_jenis_item ji','ji.id_jenis_item = i.jenis_item','inner')
			->where("nama_item like '%$search%'")
			->get('tb_item i');
	}

	function getPaketItem($id){
		return $q = $this->db->select('i.*')
			->join('tb_upacara u','u.id_upacara = p.upacara_id', 'inner')
			->join('tb_paket_item pi','pi.paket_id = p.id_paket', 'inner')
			->join('tb_item i','i.id_item = pi.item_id', 'inner')
			->where('p.upacara_id', $id)
			->get('tb_paket p');
	}

	public function getAllpromo()
	{
		return $q = $this->db->where('status', 'promo')->get('tb_paket');
	}
	public function getAllpromoitem()
	{
		return $q = $this->db->where('status', 'promo')->get('tb_item');
	}

	function getItem($id_jenis_item){
      return $this->db->join('tb_jenis_item','tb_jenis_item.id_jenis_item=tb_item.jenis_item','inner')
                      ->join('tb_user','tb_user.id_user=tb_item.id_supplier','inner')
                      ->where('tb_item.jenis_item',$id_jenis_item)
                      ->where('tb_item.status','tidak promo')
                      ->get('tb_item');
    }

    function getItemById($id_item){
    	return $this->db->join('tb_jenis_item','tb_jenis_item.id_jenis_item=tb_item.jenis_item','inner')
                      	->where('tb_item.id_item',$id_item)
                      	->get('tb_item');
    }

    function getPaketItemById($id_item){
    	return $this->db->join('tb_paket_item','tb_paket_item.id_paket=tb_paket.id_paket','inner')
    					->join('tb_item','tb_paket_item.id_item=tb_item.id_item','inner')
    					->where('tb_paket.id_paket',$id_item)
    					->get('tb_paket');
    }

    function getPaketById($id_item){
    	return $this->db->where('id_paket',$id_item)->get('tb_paket');
    }

    function newDetailTransaksi($data){
    	return $this->db->insert('tb_transaksi_detail',$data);
    }

    function newDetailPaketTransaksi($data){
    	return $this->db->insert('tb_transaksi_paket_detail',$data);
    }

     function newPaketItem($data){
    	return $this->db->insert('tb_transaksi_paket_item',$data);
    }

    function getCartItem($id_member){
    	return $q = $this->db->join('tb_transaksi_detail','tb_transaksi_detail.id_transaksi=tb_transaksi.id_transaksi','inner')
    						 ->join('tb_item','tb_item.id_item=tb_transaksi_detail.id_item','inner')
							 ->where('tb_transaksi.id_member',$id_member)
							 ->where('tb_transaksi.status_transaksi',0)
							 ->get('tb_transaksi');
    }

    function getCartItemById($id){
    	return $q = $this->db->join('tb_transaksi_detail','tb_transaksi_detail.id_transaksi=tb_transaksi.id_transaksi','inner')
    						 ->join('tb_item','tb_item.id_item=tb_transaksi_detail.id_item','inner')
							 ->where('tb_transaksi.id_transaksi',$id)
							 ->get('tb_transaksi');
    }

    function getPaketCartItem($id_member){
    	return $q = $this->db->join('tb_transaksi_paket_detail','tb_transaksi_paket_detail.id_transaksi_paket=tb_transaksi_paket.id_transaksi_paket','inner')
    						 ->join('tb_paket','tb_paket.id_paket=tb_transaksi_paket_detail.id_item')
							 ->where('tb_transaksi_paket.id_member',$id_member)
							 ->where('tb_transaksi_paket.status_transaksi',0)
							 ->get('tb_transaksi_paket');
    }

    function getPaketCartItemById($id){
    	return $q = $this->db->join('tb_transaksi_paket_detail','tb_transaksi_paket_detail.id_transaksi_paket=tb_transaksi_paket.id_transaksi_paket','inner')
    						 ->join('tb_paket','tb_paket.id_paket=tb_transaksi_paket_detail.id_item','inner')
							 ->where('tb_transaksi_paket.id_transaksi_paket',$id)
							 ->get('tb_transaksi_paket');
    }

    function updateTotalHarga($id_transaksi,$total_baru){
    	return $q = $this->db->where('id_transaksi',$id_transaksi)->update('tb_transaksi',$total_baru);
    }

    function updateTotalHargaPaket($id_transaksi,$total_baru){
    	return $q = $this->db->where('id_transaksi_paket',$id_transaksi)->update('tb_transaksi_paket',$total_baru);
    }

    function checkout($data){
    	return $q = $this->db->insert('tb_pembayaran',$data);
    }

    function updateStatusTransaksi($id_transaksi,$status_transaksi){
    	return $q = $this->db->where('id_transaksi',$id_transaksi)->update('tb_transaksi',$status_transaksi);
    }

    function updateStatusTransaksiPaket($id_transaksi_paket,$status_transaksi){
    	return $q = $this->db->where('id_transaksi_paket',$id_transaksi_paket)->update('tb_transaksi_paket',$status_transaksi);
    }

    function getPembayaran($id_member, $notif=false){
		if (!$notif) $where = "id_member = $id_member";
		else $where = "id_member = $id_member and (status_pembayaran = 0 or status_pembayaran = 1)";
		return $q = $this->db
			->where($where)
			->get('tb_pembayaran');
    }

    function upload_bukti($data,$id_pembayaran){
    	return $q = $this->db->where('id_pembayaran',$id_pembayaran)->update('tb_pembayaran',$data);
	}
	
	function getAllUpacara(){
		return $this->db->get('tb_upacara');
	}

	function getDetailPaket($id_paket){
		return $this->db->query("select *,
			  (select nama_item from tb_item where id_item=item_id) as nama_item
			  from tb_paket_item where paket_id = ".$id_paket."")->result();
	  }
}
