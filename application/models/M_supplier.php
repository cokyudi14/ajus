<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_supplier extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function get_supplier_type(){
    return $this->db->get('tb_supplier')->result();
  }

  function get_supplier(){
    $this->db->join('tb_supplier', 'tb_supplier.username = tb_user.username');
    return $this->db->get('tb_user')->result();
  }

  function add_supplier(){
    $this->db->where('id_type_supplier', $this->input->post('id_type_supplier'));
    $supp = array(
      'username' => $this->input->post('username'),
    );
    $update = $this->db->update('tb_supplier', $supp);
    if ($update) {
      return "berhasil";
    }
  }

  function edit_supplier(){
    $this->db->where('username', $this->input->post('username'));
    $supp = array(
      'username' => '',
    );
    $this->db->update('tb_supplier', $supp);

    $this->db->where('id_type_supplier', $this->input->post('supplier'));
    $supp = array(
      'username' => $this->input->post('username'),
    );
    $update = $this->db->update('tb_supplier', $supp);
    if ($update) {
      return "berhasil";
    }
  }

  function delete_supplier($username){
    $this->db->where('username', $username);
    $supp = array(
      'username' => '',
    );
    return $this->db->update('tb_supplier', $supp);
  }

}
