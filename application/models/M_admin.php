<?php
class M_admin extends CI_Model{
  	function __construct(){
          parent::__construct();
      }

    // Login

    function cek_login($username,$password){
  		$q=$this->db->where('username',$username)
  				  ->where('password',$password)
  				  ->get('tb_user');

  		if($q->num_rows()>0){
  			if ($q->row()->deleted != 0) {
  				return('block');
  			}else{
  				return($q->result());
  			}
  		}else{
  			return('false');
  		}
  	}

    function get_all_member(){
      return $this->db->get('tb_member');
    }

    function ajax_get_member($id_member){
      return $this->db->where('id_member',$id_member)->get('tb_member')->result();
    }

    function add_member(){
      $this->db->where('email', $this->input->post('email'));
      $cek = $this->db->get('tb_member')->num_rows();
      if ($cek>0) {
        return 'email_sama';
      }
      else if ($this->input->post('password')==$this->input->post('rpt_password')) {
        $data=array(
          'email' => $this->input->post('email'),
          'password' => md5($this->input->post('password')),
          'alamat' => $this->input->post('alamat'),
          'telp' => $this->input->post('telp'),
        );
        $insert = $this->db->insert('tb_member',$data);
        if ($insert) return 'berhasil';
      }
      else return 'password_beda';
    }

    function edit_member(){
      $id_member = $this->input->post('id_member');
      $data=array(
        'alamat' => $this->input->post('alamat'),
        'telp' => $this->input->post('telp'),
      );
      $this->db->where('id_member', $id_member);
      return $this->db->update('tb_member',$data);
    }

    function delete_member($id_member){
      $this->db->where('id_member', $id_member);
      return $this->db->delete('tb_member');
    }

     //upacara
    function get_all_upacara(){
      return $this->db->get('tb_upacara');
    }
    function ajax_get_upacara($id_upacara){
      return $this->db->where('id_upacara',$id_upacara)->get('tb_upacara')->result();
    }

    function add_upacara(){
      $this->db->where('nama_upacara', $this->input->post('nama_upacara'));
      $cek = $this->db->get('tb_upacara')->num_rows();
      if ($cek>0) {
        return 'upacara_sama';
      }
      else {
        $data=array(
          'nama_upacara' => $this->input->post('nama_upacara'),
        );
        $insert = $this->db->insert('tb_upacara',$data);
        if ($insert) return 'berhasil';
      }
    }
    function edit_upacara(){
      $id_upacara = $this->input->post('id_upacara');
      $data=array(
        'nama_upacara' => $this->input->post('nama_upacara'),
      );
      $this->db->where('id_upacara', $id_upacara);
      return $this->db->update('tb_upacara',$data);
    }
     function delete_upacara($id_upacara){
      $this->db->where('id_upacara', $id_upacara);
      return $this->db->delete('tb_upacara');
    }

     //Paket

    function ajax_get_paket($id_paket){
      return $this->db->query("select *,
            (select nama_upacara from tb_upacara where id_upacara=upacara_id) as nama_upacara
            from tb_paket where id_paket = ".$id_paket."")->result();
    }

    function item_paket($id_paket){
        $this->db->select('item_id');
        $this->db->where('paket_id',$id_paket);
        $this->db->from('tb_paket_item');
        return $this->db->get();
    }

    function get_all_paket($status){

      return $this->db->query("select *,
            (select nama_upacara from tb_upacara where id_upacara=upacara_id) as nama_upacara
            from tb_paket where status='".$status."' ");
    }
    function get_nama_paket($id_paket){
      $this->db->select('id_paket,nama_paket');
      $this->db->where('id_paket', $id_paket);
      $query= $this->db->get('tb_paket');
      return $query->result();
    }
    function get_item_paket($id_paket){
      return $this->db->query("select *,
            (select deskripsi from tb_item where id_item=item_id) as deskripsi,
            (select nama_item from tb_item where id_item=item_id) as nama_item
            from tb_paket_item where paket_id = ".$id_paket."")->result();
    }
     function get_detail_item_paket($id_paket){
      return $this->db->query("select *,
            (select nama_item from tb_item where id_item=item_id) as nama_item
            from tb_paket_item where paket_id = ".$id_paket."")->result();
    }

     function add_paket($add){
        $insert = $this->db->insert('tb_paket',$add);
        if ($insert) return 'berhasil';
    }
    function delete_paket($id_paket){
      $this->db->where('id_paket', $id_paket);
      return $this->db->delete('tb_paket');
    }

     function delete_item_paket($id_paket_item){
      $this->db->where('id_paket_item', $id_paket_item);
      return $this->db->delete('tb_paket_item');
    }

    function edit_paket($update,$id_paket){
      $q= $this->db->where('id_paket', $id_paket)
               ->update('tb_paket',$update);
      if($q) return 'berhasil';
    }

    function add_item_paket($add){
        $insert = $this->db->insert('tb_paket_item',$add);
        if ($insert) return 'berhasil';
    }



    //jenis item
    function get_all_jenis_item(){

      return $this->db->get('tb_jenis_item');
    }

    function ajax_get_jenis_item($id_jenis_item){
      return $this->db->where('id_jenis_item',$id_jenis_item)->get('tb_jenis_item')->result();
    }

    function add_jenis_item(){
      $this->db->where('jenis_item', $this->input->post('jenis_item'));
      $cek = $this->db->get('tb_jenis_item')->num_rows();
      if ($cek>0) {
        return 'jenis_item_sama';
      }
      else {
        $data=array(
          'jenis_item' => $this->input->post('jenis_item'),
        );
        $insert = $this->db->insert('tb_jenis_item',$data);
        if ($insert) return 'berhasil';
      }
    }

    function edit_jenis_item(){
      $id_jenis_item = $this->input->post('id_jenis_item');
      $data=array(
        'jenis_item' => $this->input->post('jenis_item'),
      );
      $this->db->where('id_jenis_item', $id_jenis_item);
      return $this->db->update('tb_jenis_item',$data);
    }

    function delete_jenis_item($id_jenis_item){
      $this->db->where('id_jenis_item', $id_jenis_item);
      return $this->db->delete('tb_jenis_item');
    }

    //item
    function get_all_item($status){
      $this->db->join('tb_jenis_item','tb_jenis_item.id_jenis_item=tb_item.jenis_item','inner')
                      ->join('tb_user','tb_user.id_user=tb_item.id_supplier');
      $this->db->where('item_status','diterima');
      if ($status!='all') {
        $this->db->where('status',$status);
      }
      return $this->db->get('tb_item');
    }

    function get_all_KonfirmasiItem(){
      $this->db->join('tb_jenis_item','tb_jenis_item.id_jenis_item=tb_item.jenis_item','inner')
                      ->join('tb_user','tb_user.id_user=tb_item.id_supplier');
      $this->db->where('item_status','tertunda');
      return $this->db->get('tb_item');
    }

    function get_supp_item($id_supp){
      return $this->db->join('tb_jenis_item','tb_jenis_item.id_jenis_item=tb_item.jenis_item','inner')
                      ->join('tb_user','tb_user.id_user=tb_item.id_supplier')
                      ->where('id_supplier',$id_supp)
                      ->get('tb_item');
    }

    function get_IdItem($id_supp){
        $this->db->select('id_item');
        $this->db->where('id_supplier',$id_supp);
        $this->db->from('tb_item');
        return $this->db->get();
    }
    function get_pemesanan_item($id_item){
      // return $this->db->query("select * from tb_transaksi_detail where ".$item."");
       return $this->db->join('tb_item','tb_item.id_item=tb_transaksi_detail.id_item','inner')
                       ->join('tb_transaksi','tb_transaksi.id_transaksi=tb_transaksi_detail.id_transaksi','inner')
                       ->join('tb_member','tb_member.id_member=tb_transaksi.id_member')
                       ->where('tb_transaksi_detail.id_item',$id_item)->get('tb_transaksi_detail');
    }
    function get_pemesanan_paket_item($id_item){
      // return $this->db->query("select * from tb_transaksi_detail where ".$item."");
       return $this->db->join('tb_item','tb_item.id_item=tb_transaksi_paket_item.id_item','inner')
                       ->join('tb_transaksi_paket','tb_transaksi_paket.id_transaksi_paket=tb_transaksi_paket_item.id_transaksi_paket','inner')
                       ->where('tb_transaksi_paket_item.id_item',$id_item)->get('tb_transaksi_paket_item');
    }

    function get_pemesanan_detail($id_transaksi_detail){
      // return $this->db->query("select * from tb_transaksi_detail where ".$item."");
       return $this->db->join('tb_item','tb_item.id_item=tb_transaksi_detail.id_item','inner')
                       ->join('tb_transaksi','tb_transaksi.id_transaksi=tb_transaksi_detail.id_transaksi')
                       ->join('tb_member','tb_member.id_member=tb_transaksi.id_member')
                       ->where('tb_transaksi_detail.id_transaksi_detail',$id_transaksi_detail)->get('tb_transaksi_detail');
    }
     function get_pemesanan_detail_paket($id_transaksi_detail){
      // return $this->db->query("select * from tb_transaksi_detail where ".$item."");
       return $this->db->join('tb_item','tb_item.id_item=tb_transaksi_paket_item.id_item','inner')
                       ->join('tb_transaksi_paket','tb_transaksi_paket.id_transaksi_paket=tb_transaksi_paket_item.id_transaksi_paket')
                       ->where('tb_transaksi_paket_item.id_transaksi_paket_item',$id_transaksi_detail)->get('tb_transaksi_paket_item');
    }

    function ajax_get_item($id_item){
      return $this->db->join('tb_jenis_item','tb_jenis_item.id_jenis_item=tb_item.jenis_item','inner')
                      ->join('tb_user','tb_user.id_user=tb_item.id_supplier')
                      ->where('tb_item.id_item',$id_item)
                      ->get('tb_item')->result();
    }

    function add_item($add){
        $insert = $this->db->insert('tb_item',$add);
        if ($insert) return 'berhasil';
    }

    function edit_item($update,$id_item){
      $q= $this->db->where('id_item', $id_item)
               ->update('tb_item',$update);
      if($q) return 'berhasil';
    }

    function delete_item($id_item){
      $this->db->where('id_item', $id_item);
      return $this->db->delete('tb_item');
    }

    function get_supplier(){
      $this->db->join('tb_jenis_item', 'tb_jenis_item.id_jenis_item = tb_user.id_jenis_item')->where('id_role','supplier');
      return $this->db->get('tb_user')->result();
    }

    function ajax_get_supp(){
      $this->db->where('tb_user.username', $this->input->post('username'));
      $this->db->join('tb_jenis_item', 'tb_jenis_item.id_jenis_item = tb_user.id_jenis_item');
      return $this->db->get('tb_user')->result();
    }

    function get_jenis_item(){
      return $this->db->get('tb_jenis_item');
    }

    function getAllTransaksi(){
      return $this->db->join('tb_member','tb_member.id_member=tb_transaksi.id_member','inner')
                      ->get('tb_transaksi');
    }
    function getAllTransaksiPaket(){
      return $this->db->join('tb_member','tb_member.id_member=tb_transaksi_paket.id_member','inner')
                      ->get('tb_transaksi_paket');
    }


  //konfirmasi

    function get_all_konfirmasi(){

    return $this->db->where('status_pembayaran!=','0' )->get('tb_pembayaran');

    }

     function getItemById($id){
      return $q = $this->db->join('tb_transaksi_detail','tb_transaksi_detail.id_transaksi=tb_transaksi.id_transaksi','inner')
                 ->join('tb_item','tb_item.id_item=tb_transaksi_detail.id_item','inner')
               ->where('tb_transaksi.id_transaksi',$id)
               ->get('tb_transaksi');
    }
     function getPaketById($id){
      return $q = $this->db->join('tb_transaksi_paket_detail','tb_transaksi_paket_detail.id_transaksi_paket=tb_transaksi_paket.id_transaksi_paket','inner')
                 ->join('tb_paket','tb_paket.id_paket=tb_transaksi_paket_detail.id_item','inner')
               ->where('tb_transaksi_paket.id_transaksi_paket',$id)
               ->get('tb_transaksi_paket');
    }
  // transaksi

    function ajax_get_transaksi_paket($id_transaksi_paket){
     return $this->db->query("select *,
            (select nama_paket from tb_paket where id_paket=id_item) as nama_paket
            from tb_transaksi_paket_detail where id_transaksi_paket = ".$id_transaksi_paket."")->result();
    }
     function ajax_get_transaksi($id_transaksi){
     return $this->db->where('id_transaksi',$id_transaksi)->get('tb_transaksi')->result();
    }

  //notif
     function notif($notif){
        $insert = $this->db->insert('tb_notif',$notif);
    }
     function get_IdSupp($id_item){
        $this->db->select('id_supplier');
        $this->db->where('id_item',$id_item);
        $this->db->from('tb_item');
        return $this->db->get();
    }
     function get_notif($id_supp){
      return $this->db->join('tb_item','tb_item.id_item=tb_notif.item_id')
                      ->where('supplier_id',$id_supp)
                      ->where('status_notif','terkirim')
                      ->get('tb_notif');
    }

    function update_notif($notif,$id_notif){
       $this->db->where('id_notif', $id_notif);
      return $this->db->update('tb_notif',$notif);

    }
  //profil
    function profil ($id_supp){
      $this->db->join('tb_jenis_item','tb_jenis_item.id_jenis_item=tb_user.id_jenis_item')->where('tb_user.id_user', $id_supp);
      return $this->db->get('tb_user')->result();
    }


}
?>
