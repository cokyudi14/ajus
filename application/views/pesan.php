<!--Main layout-->
  <main class="mt-5 pt-4">
    <div class="container dark-grey-text mt-5">

      <!--Grid row-->
      <div class="row wow fadeIn">

        <!--Grid column-->
        <div class="col-md-6 mb-4">

          <img src="<?=base_url("assets/images/item/$item->foto_item")?>" class="img-fluid" alt="">

        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-md-6 mb-4">

          <!--Content-->
          <div class="p-4">

            <div class="mb-3">
              <h1><span class="badge indigo mr-1"><?=$item->nama_item?></span></h1>
              <span class="badge cyan mr-1"><?=$item->jenis_item?></span>
            </div>

            <p class="lead">
              <span>Rp. <?=number_format($item->harga_jual,'0','.','.');?></span>
            </p>

            <p class="lead font-weight-bold">Deskripsi</p>

            <p><?=$item->deskripsi?></p>

            <form action="<?=site_url('Pesanan/masukChart')?>" method="post">
              <!-- Default input -->
            <div class="row">
              <div class="col-md-12">
                  <label>Tanggal Upacara</label>
                  <div class="input-group date" data-provide="datepicker">
                      <input type="text" name="tgl_upacara" placeholder="Tanggal Upacara" class="form-control" required>
                      <div class="input-group-addon">
                          <span class="glyphicon glyphicon-th"></span>
                      </div>
                  </div>
                  <br>
                  <label>Jumlah</label>
                  <input type="number" min="1" value="1" name="qty_item" class="form-control" required="">
                  <input type="hidden" value="2" name="jenis_item" class="form-control">
                  <input type="hidden" value="<?=$item->id_item?>" name="id_item" class="form-control">
                  <input type="hidden" value="<?=$item->harga_jual?>" name="harga" class="form-control">
                  <br>
              </div>
              <div class="col-md-12">
                <button class="btn btn-primary btn-md my-0 p pull-right" type="submit">Add to cart
                  <i class="fa fa-shopping-cart ml-1"></i>
                </button>
              </div>
            </div>
            </form>

          </div>
          <!--Content-->

        </div>
        <!--Grid column-->

      </div>
      <!--Grid row-->

      <hr>

      <!--Grid row-->
      <div class="row d-flex justify-content-center wow fadeIn" style="margin-bottom: 180px;">



      </div>
      <!--Grid row-->

    </div>
  </main>
  <!--Main layout-->
  <script type="text/javascript">
    $('.datepicker').datepicker();
  </script>
