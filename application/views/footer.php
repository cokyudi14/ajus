
  <!--Footer-->
  <footer class="page-footer text-center font-small mt-4 wow fadeIn">
    <!-- Social icons -->

    <!--Copyright-->
    <div class="footer-copyright py-3">
      © 2018 Copyright:
      <a href="" target="_blank"> E-Organizer </a> | 
      admin@eorganizer.com | 0812345678
    </div>
    <!--/.Copyright-->

  </footer>
  <!--/.Footer-->


  <!-- SCRIPTS -->
  <!-- JQuery -->
  <script type="text/javascript" src="<?php echo base_url('assets/mdbootstrap/js/jquery-3.3.1.min.js')?>"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="<?php echo base_url('assets/mdbootstrap/js/popper.min.js')?>"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="<?php echo base_url('assets/mdbootstrap/js/bootstrap.min.js')?>"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="<?php echo base_url('assets/mdbootstrap/js/mdb.min.js')?>"></script>
  <!-- Initializations -->
  <!-- DataTables -->
<script src="<?php echo base_url('assets/plugins/datatables/jquery.dataTables.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/plugins/datatables/dataTables.bootstrap.min.js"'); ?>" type="text/javascript"></script>
  <script type="text/javascript">
    // Animations initialization
    new WOW().init();

    $('#regBtn').click(function(){
      $('#modalLogin').modal('hide');
      $('#modalRegister').modal();
    });
  </script>

<?php if($this->session->flashdata('memberLogin')){ ?>
      <script type="text/javascript">
        $(window).on('load',function(){
          $('#modalLogin').modal('show');
        });
      </script>
<?php } ?>
<?php if($this->session->flashdata('memberRegister')){ ?>
      <script type="text/javascript">
        $(window).on('load',function(){
          $('#modalRegister').modal('show');
        });
      </script>
<?php } ?>
<?php if($this->session->flashdata('memberCheckout')){ ?>
      <script type="text/javascript">
        $(window).on('load',function(){
          $('#modalCheckout').modal('show');
        });
      </script>
<?php } ?>
<?php if($this->session->flashdata('bukti_pembayaran')){ ?>
      <script type="text/javascript">
        $(window).on('load',function(){
          $('#modalBukti').modal('show');
        });
      </script>
<?php } ?>
</body>

</html>