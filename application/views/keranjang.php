<main class="mt-5 pt-4">
    <div class="container wow fadeIn">

      <!-- Heading -->
      <h2 class="my-5 h2 text-center">Checkout form</h2>

      <!--Grid row-->
      <div class="row">

        <!--Grid column-->
        <div class="col-md-8 mb-4">

          <!--Card-->
          <div class="card">

            <!--Card content-->
            <form action="<?=site_url('/keranjang/checkout')?>" method="post" class="card-body">

              <div class="d-block my-3">
                <label for="cc-name"><b>Bank Tujuan</b></label>
                <div class="custom-control custom-radio">
                  <input id="mandiri" name="bank_tujuan" value="mandiri" type="radio" class="custom-control-input" checked required>
                  <label class="custom-control-label" for="mandiri">Mandiri (Rek: 19112121212)</label>
                </div>
                <div class="custom-control custom-radio">
                  <input id="bca" name="bank_tujuan" value="bca" type="radio" class="custom-control-input" required>
                  <label class="custom-control-label" for="bca">BCA (Rek: 19112121212)</label>
                </div>
                <div class="custom-control custom-radio">
                  <input id="bpd" name="bank_tujuan" value="bpd" type="radio" class="custom-control-input" required>
                  <label class="custom-control-label" for="bpd">BPD (Rek: 19112121212)</label>
                </div>
              </div>
              <hr class="mb-4">
              <input type="hidden" class="form-control" name="id_transaksi" value="<?=$id_transaksi?>">
              <input type="hidden" class="form-control" name="id_transaksi_paket" value="<?=$id_transaksi_paket?>">
    <?php if(count($chart)+count($paketChart)==0){  ?>
              <button class="btn btn-danger btn-lg btn-block" type="submit" disabled="">Keranjang Kosong</button>
    <?php }else{ ?>
              <button class="btn btn-primary btn-lg btn-block" type="submit">Continue to checkout</button>
    <?php }?>
            </form>

          </div>
          <!--/.Card-->

        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-md-4 mb-4">

          <!-- Heading -->
          <h4 class="d-flex justify-content-between align-items-center mb-3">
            <span class="text-muted">Your cart</span>
            <span class="badge badge-secondary badge-pill"><?=$this->session->userdata('chart')?></span>
          </h4>

          <!-- Cart -->
          <ul class="list-group mb-3 z-depth-1">
      <?php foreach ($chart as $key => $value) { ?>
            <li class="list-group-item d-flex justify-content-between lh-condensed">
              <div>
                <h6 class="my-0"><?=$value->nama_item?> (<?=$value->qty_item?>)</h6>
                <small class="text-muted">Tanggal Upacara: <b><?=$value->tgl_upacara?></b></small>
              </div>
              <span class="text-muted">Rp. <?php echo number_format($value->harga_jual*$value->qty_item,0,".",".")  ?></span>
            </li>
      <?php } ?>
      <?php foreach ($paketChart as $key => $value) { ?>
            <li class="list-group-item d-flex justify-content-between lh-condensed">
              <div>
                <h6 class="my-0"><?=$value->nama_paket?></h6>
                <small class="text-muted">Tanggal Upacara: <b><?=$value->tgl_upacara?></b></small>
              </div>
              <span class="text-muted">Rp. <?php echo number_format($value->harga_paket,0,".",".")  ?></span>
            </li>
      <?php } ?>
            <!-- <li class="list-group-item d-flex justify-content-between bg-light">
              <div class="text-success">
                <h6 class="my-0">Promo code</h6>
                <small>EXAMPLECODE</small>
              </div>
              <span class="text-success">-$5</span>
            </li> -->
            <li class="list-group-item d-flex justify-content-between">
              <span>Total</span>
              <strong>Rp. <?php echo number_format($total,0,".",".")  ?></strong>
            </li>
          </ul>
          <!-- Cart -->

          <!-- Promo code -->
          <!-- <form class="card p-2">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Promo code" aria-label="Recipient's username" aria-describedby="basic-addon2">
              <div class="input-group-append">
                <button class="btn btn-secondary waves-effect m-0" type="button">Redeem</button>
              </div>
            </div>
          </form> -->
          <!-- Promo code -->

        </div>
        <!--Grid column-->

      </div>
      <!--Grid row-->

    </div>
  </main>
  <!--Main layout-->
