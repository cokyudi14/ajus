<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>E-Organizer</title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url('assets/mdbootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="<?php echo base_url('assets/mdbootstrap/css/mdb.min.css')?>" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="<?php echo base_url('assets/mdbootstrap/css/style.min.css')?>" rel="stylesheet">
  <link href="<?php echo base_url('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')?>" rel="stylesheet">

  <script type="text/javascript" src="<?php echo base_url('assets/mdbootstrap/js/jquery-3.3.1.min.js')?>"></script>
  <script type="text/javascript" src="<?php echo base_url('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')?>"></script>
  <style type="text/css">
    html,
    body,
    header,
    .carousel {
      height: 60vh;
    }

    @media (max-width: 740px) {
      html,
      body,
      header,
      .carousel {
        height: 100vh;
      }
    }

    @media (min-width: 800px) and (max-width: 850px) {
      html,
      body,
      header,
      .carousel {
        height: 100vh;
      }
    }

    .navbar-nav .nav-item {
      margin: 0 8px;
    }
  </style>
</head>

<body>

  <!-- Navbar -->
  <nav class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
    <div class="container">

      <!-- Brand -->
      <a class="navbar-brand waves-effect" href="<?=site_url()?>">
        <strong class="blue-text">E-Organizer</strong>
      </a>

      <!-- Collapse -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <!-- Links -->
      <div class="collapse navbar-collapse" id="navbarSupportedContent">

        <!-- Left -->
        <ul class="navbar-nav mr-auto">
          <!-- <li class="nav-item active">
            <a class="nav-link waves-effect" href="#">Home</a>
          </li> -->
        </ul>

        <!-- Right -->
        <ul class="navbar-nav nav-flex-icons">
    <?php if($this->session->userdata('id_member')){ ?>
          <li class="nav-item">
            <a href="<?=site_url('/keranjang')?>" class="nav-link waves-effect">
      <?php if($this->session->userdata('chart')){ ?>
              <span class="badge red z-depth-1 mr-1"><?=$this->session->userdata('chart')?></span>
      <?php } ?>
              <i class="fa fa-shopping-cart"></i>
              <span class="clearfix d-none d-sm-inline-block"> Cart </span>
            </a>
          </li>
    <?php } ?>

    <?php if(!$this->session->userdata('id_member')){ ?>
          <li class="nav-item">
            <a class="nav-link border border-light rounded waves-effect" target="_blank" data-toggle="modal" data-target="#modalLogin">
              <i class="fa fa-sign-in mr-2"></i>Login
            </a>
          </li>
    <?php } ?>
    <?php if($this->session->userdata('id_member')){ ?>
          <li class="nav-item">
            <div class="btn-group">
                <a class="nav-link border border-light rounded waves-effect dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <?php if ($notif_payment > 0): ?>
                    <span class="badge red z-depth-1 mr-1"><?= $notif_payment ?></span>
                  <?php endif; ?>
                  <i class="fa fa-user mr-2"></i><?=$this->session->userdata('nama_member')?>
                </a>

                <div class="dropdown-menu">
                    <a class="dropdown-item" href="<?=site_url('/Profil')?>">Profil</a>
                    <a class="dropdown-item" href="<?=site_url('/pembayaran')?>">
                      Pesanan Saya
                      <span class="badge red z-depth-1 mr-1"><?= $notif_payment ?></span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="<?=site_url('/Home/logout')?>">Log Out</a>
                </div>
            </div>
          </li>
    <?php } ?>
        </ul>

      </div>

    </div>
  </nav>
  <!-- Navbar -->

  <div class="modal" id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <!--Modal: Contact form-->
    <div class="modal-dialog cascading-modal" role="document">

        <!--Content-->
        <div class="modal-content">

            <!--Header-->
            <div class="modal-header primary-color white-text">
                <h4 class="title">
                    <i class="fa fa-pencil"></i> Login</h4>
                <button type="button" class="close waves-effect waves-light" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <!--Body-->
            <div class="modal-body">

              <form action="<?=site_url('/Home/login')?>" method="post">
                <?php if($this->session->flashdata('memberLogin')){ ?>
                      <?=$this->session->flashdata('memberLogin')?>
                <?php } ?>
                <!-- Material input name -->
                <div class="md-form form-sm">
                    <i class="fa fa-envelope prefix"></i>
                    <input type="text" name="email" class="form-control form-control-sm">
                    <label>Email</label>
                </div>

                <!-- Material input email -->
                <div class="md-form form-sm">
                    <i class="fa fa-lock prefix"></i>
                    <input type="password" name="password" class="form-control form-control-sm">
                    <label>Password</label>
                </div>

                <div class="text-center mt-4 mb-2">
                    <button type="submit" class="btn btn-primary">Login
                        <i class="fa fa-send ml-2"></i>
                    </button>
                </div>
              </form>
                <div class="text-center black-text mx-5 wow fadeIn">
                  <p>
                    Belum punya akun? <a class="blue-text" id="regBtn">Register</a>
                  </p>
                </div>

            </div>
        </div>
        <!--/.Content-->
    </div>
    <!--/Modal: Contact form-->
  </div>

  <div class="modal" id="modalRegister" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <!--Modal: Contact form-->
    <div class="modal-dialog cascading-modal" role="document">

        <!--Content-->
        <div class="modal-content">

            <!--Header-->
            <div class="modal-header special-color white-text">
                <h4 class="title">
                    <i class="fa fa-pencil"></i> Register</h4>
                <button type="button" class="close waves-effect waves-light" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <!--Body-->
            <div class="modal-body">
              <form action="<?=site_url('Home/register')?>" method="post">
                <?php if($this->session->flashdata('memberRegister')){ ?>
                      <?=$this->session->flashdata('memberRegister')?>
                <?php } ?>
                <!-- Material input name -->
                <div class="md-form form-sm">
                    <input type="email" name="email" class="form-control form-control-sm" required>
                    <label>Email</label>
                </div>

                <!-- Material input email -->
                <div class="md-form form-sm">
                    <input type="password" name="password" class="form-control form-control-sm" required>
                    <label>Password</label>
                </div>

                <!-- Material input subject -->
                <div class="md-form form-sm">
                    <input type="text" name="nama_member" class="form-control form-control-sm" required>
                    <label>Nama</label>
                </div>

                <div class="md-form form-sm">
                    <input type="text" name="kota" class="form-control form-control-sm" required>
                    <label>Kota</label>
                </div>

                <div class="md-form form-sm">
                  <input type="number" name="telp" class="form-control form-control-sm" required>
                  <label>Telepon</label>
                </div>
                
                <div class="md-form form-sm">
                    <input type="text" name="kode_pos" class="form-control form-control-sm" required>
                    <label>Kode Pos</label>
                </div>

                <!-- Material textarea message -->
                <div class="md-form form-sm">
                    <textarea type="text" name="alamat" class="md-textarea form-control" required></textarea>
                    <label>Alamat</label>
                </div>

                <div class="text-center mt-4 mb-2">
                    <button type="submit" class="btn btn-primary">Register</button>
                </div>
              </form>

            </div>
        </div>
        <!--/.Content-->
    </div>
    <!--/Modal: Contact form-->
  </div>

  <div class="modal" id="modalCheckout" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <!--Modal: Contact form-->
    <div class="modal-dialog cascading-modal" role="document">

        <!--Content-->
        <div class="modal-content">

            <!--Header-->
            <div class="modal-header special-color white-text">
                <h4 class="title">
                    <i class="fa fa-pencil"></i> Ulasan</h4>
                <button type="button" class="close waves-effect waves-light" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <!--Body-->
            <div class="modal-body">
              <?php if($this->session->flashdata('memberCheckout')){ ?>
                      <?=$this->session->flashdata('memberCheckout')?>
                <?php } ?>
              <p>Checkout berhasil, silahkan menuju halaman daftar pesanan untuk melakukan upload bukti pembayaran anda, Anda memiliki waktu 1 hari untuk melakukan upload bukti pembayaran.</p>
            </div>
        </div>
        <!--/.Content-->
    </div>
    <!--/Modal: Contact form-->
  </div>

  <div class="modal" id="modalBukti" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <!--Modal: Contact form-->
      <div class="modal-dialog cascading-modal" role="document">

          <!--Content-->
          <div class="modal-content">

              <!--Header-->
              <div class="modal-header special-color white-text">
                  <h4 class="title">
                      <i class="fa fa-pencil"></i> Ulasan</h4>
                  <button type="button" class="close waves-effect waves-light" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                  </button>
              </div>
              <!--Body-->
              <div class="modal-body">
                <?php if($this->session->flashdata('bukti_pembayaran')){ ?>
                        <?=$this->session->flashdata('bukti_pembayaran')?>
                  <?php } ?>
          </div>
          <!--/.Content-->
      </div>
      <!--/Modal: Contact form-->
    </div>
  </div>
