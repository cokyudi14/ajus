
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <section class="content-header">
      <?php if ($this->session->flashdata('success')): ?>
        <div class="callout callout-success lead">
          <h4>Berhasil !</h4>
          <p><?php echo $this->session->flashdata('success')?></p>
        </div>
      <?php endif; ?>
      <h1>
        Kelola Transaksi
  
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box box-solid box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">List Pesanan Barang Terpisah</h3>
        </div>
        <div class="box-body">
          <table class="table table1 table-striped table-bordered table-hover" id='tb_list_item'>
            <thead>
              <tr>
                <th>Id Transaksi</th>
                <th>Nama Item</th>
                <th>Nama Pemesan</th>
                <th>Harga</th>
                <th>Tanggal Transaksi</th>
                <th>Tanggal Upacara</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php $no=1; ?>
              <?php foreach ($item as $key) {?>
                <tr>
                  <td>TRX-<?php echo $key->id_transaksi_detail ?></td>
                  <td><?php echo $key->nama_item ?></td>
                  <td><?php echo $key->nama_member ?></td>
                  <td>Rp. <?php echo number_format($key->harga,0,".",".")  ?></td>
                  <td><?php echo $key->tgl_transaksi ?></td>
                  <td><?php echo $key->tgl_upacara ?></td>
                  <td class="btn-group">
                    <a 
                      href="javascript:void(0)" 
                      title="Detail" 
                      class="btn btn-primary" 
                      onclick="detail_pesanan(
                        '<?php echo $key->id_transaksi_detail ?>',
                        '<?= $key->kota ?>',
                        '<?= $key->kode_pos ?>',
                        '<?= $key->telp ?>',
                        '<?= $key->alamat ?>'
                      )"><i class="fa fa-eye"></i>  Detail 
                    </a>
                   
                  </td>
                </tr>
                <?php $no++; ?>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </section>

     <!-- Main content -->
    <section class="content">
      <div class="box box-solid box-warning">
        <div class="box-header with-border">
          <h3 class="box-title">List Pesanan Paketan</h3>
        </div>
        <div class="box-body">
          <table class="table table1 table-striped table-bordered table-hover" id='tb_list_item2'>
            <thead>
              <tr>
                <th>Id Transaksi</th>
                <th>Nama Item</th>
                <th>Harga</th>
                <th>Tanggal Transaksi</th>
                <th>Tanggal Upacara</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php $no=1; ?>
              <?php foreach ($item_p as $key) {?>
                <tr>
                  <td>TRX-<?php echo $key->id_transaksi_paket_item ?></td>
                  <td><?php echo $key->nama_item ?></td>
                  <td>Rp. <?php echo number_format($key->harga_modal,0,".",".")  ?></td>
                   <td><?php echo $key->tgl_transaksi ?></td>
                  <td><?php echo $key->tgl_upacara ?></td>
                  <td class="btn-group">
                    <a href="javascript:void(0)" title="Detail" class="btn btn-primary" onclick="detail_pesanan2('<?php echo $key->id_transaksi_paket_item ?>')"><i class="fa fa-eye"></i>  Detail </a>
                   
                  </td>
                </tr>
                <?php $no++; ?>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </section>

   
    <div class="modal fade" id="modal_detal" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-md" role="document">
        <!--Content-->
        <div class="modal-content">
          <!--Header-->
          <div class="modal-header" style="background-color: #367fa9;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 id="header" class="modal-title" style="color:white">  Detail Pemesanan</h4>
          </div>

          <!--Body-->
          <div class="modal-body">
            <form id="" class="" action="<?php echo base_url('Admin/Item/update_item') ?>" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <label for="username">Nama Item</label>
                <input type="text" class="form-control" id="nama_item" name="nama_item"  readonly>
              </div>
               <div class="form-group">
                <label for="username">Nama Pemesan</label>
                <input type="text" class="form-control" id="nama_member" name="nama_item"  readonly>
              </div>
               <div class="form-group">
                <label for="username">Harga</label>
                <input type="text" class="form-control" id="harga" name="nama_item"  readonly>
              </div>
               <div class="form-group">
                <label for="username">Tanggal Upacara</label>
                <input type="text" class="form-control" id="tgl_upacara" name="nama_item"  readonly>
              </div>
               <div class="form-group">
                <label >Note</label>
                <textarea class="form-control" readonly id="note" ></textarea>
              </div>

              <div class="form-group">
                <label for="username">Kota</label>
                <input type="text" class="form-control" id="kota" readonly>
              </div>
              <div class="form-group">
                <label for="username">Kode Pos</label>
                <input type="text" class="form-control" id="kode_pos" readonly>
              </div>
              <div class="form-group">
                <label for="username">Nomor Telepon</label>
                <input type="text" class="form-control" id="telp" readonly>
              </div>
              <div class="form-group">
                <label for="username">Alamat</label>
                <textarea type="text" class="form-control" id="alamat" readonly></textarea>
              </div>
             
             <button type="button" class="btn btn-danger btn-flat pull-right" data-dismiss="modal">tutup</button>
            
             <br>
             <br>
               
             
            </form>
          </div>
          <!--Footer-->
        </div>
        <!--/.Content-->
      </div>
    </div>

 <div class="modal fade" id="modal_detal2" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-md" role="document">
        <!--Content-->
        <div class="modal-content">
          <!--Header-->
          <div class="modal-header" style="background-color: #367fa9;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 id="header" class="modal-title" style="color:white">  Detail Pemesanan</h4>
          </div>

          <!--Body-->
          <div class="modal-body">
            <form id="" class="" action="<?php echo base_url('Admin/Item/update_item') ?>" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <label for="username">Nama Item</label>
                <input type="text" class="form-control" id="nama_item2"  readonly>
              </div>
               <div class="form-group">
                <label for="username">Deskripsi</label>
                <input type="text" class="form-control" id="des" name="nama_item"  readonly>
              </div>
               <div class="form-group">
                <label for="username">Harga</label>
                <input type="text" class="form-control" id="harga2" name="nama_item"  readonly>
              </div>
               <div class="form-group">
                <label for="username">Tanggal Upacara</label>
                <input type="text" class="form-control" id="tgl_upacara2" name="nama_item"  readonly>
              </div>
             
             <button type="button" class="btn btn-danger btn-flat pull-right" data-dismiss="modal">tutup</button>
            
             <br>
             <br>
               
             
            </form>
          </div>
          <!--Footer-->
        </div>
        <!--/.Content-->
      </div>
    </div>
   

  </div>
  <!-- /.content-wrapper -->
 <!--  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; 2018</strong> All rights
    reserved.
  </footer> -->
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('bower_components/jquery/dist/jquery.min.js') ?>"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url('bower_components/jquery-ui/jquery-ui.min.js') ?>"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('bower_components/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
<!-- Morris.js charts -->
<script src="<?php echo base_url('bower_components/raphael/raphael.min.js') ?>"></script>
<script src="<?php echo base_url('bower_components/morris.js/morris.min.js') ?>"></script>
<!-- Sparkline -->
<script src="<?php echo base_url('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') ?>"></script>
<!-- jvectormap -->
<script src="<?php echo base_url('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') ?>"></script>
<script src="<?php echo base_url('plugins/jvectormap/jquery-jvectormap-world-mill-en.js') ?>"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url('bower_components/jquery-knob/dist/jquery.knob.min.js') ?>"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url('bower_components/moment/min/moment.min.js') ?>"></script>
<script src="<?php echo base_url('bower_components/bootstrap-daterangepicker/daterangepicker.js') ?>"></script>
<!-- datepicker -->
<script src="<?php echo base_url('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') ?>"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') ?>"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url('bower_components/jquery-slimscroll/jquery.slimscroll.min.js') ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('bower_components/fastclick/lib/fastclick.js') ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('dist/js/adminlte.min.js') ?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url('dist/js/pages/dashboard.js') ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('dist/js/demo.js') ?>"></script>
<script src="<?php echo base_url('bower_components/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo base_url('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>

<script>
  $.widget.bridge('uibutton', $.ui.button);
  $(document).ready(function() {
    $('#btn_pesanan').addClass('active');
    $('#tb_list_item').DataTable({
    "order": [ [ 0, 'desc' ]]
} );
    $('#tb_list_item2').DataTable({
    "order": [ [ 0, 'desc' ]]
} );
    $('#tittle').text('SIM | List Item');
  });

  $('#btn_add_item').click(function() {
    $('#modal_add_item').modal('show');
  });

function detail_pesanan(id_transaksi_detail, kota, kode_pos, telp, alamat){
    $.ajax({
      url: '<?php echo base_url('Admin/Pemesanan/ajax_get_detail_transaksi') ?>',
      type: 'POST',
      dataType: 'JSON',
      data: {
      id_transaksi_detail: id_transaksi_detail,
      },
      success:function(data){
        console.log(data);
        $('#nama_item').val(data.nama_item);
        $('#nama_member').val(data.nama_member);
        $('#harga').val('Rp. '+data.harga);
        $('#tgl_upacara').val(data.tgl_upacara);
        $('#note').val(data.pesan_detail);
        $('#kota').val(kota);
        $('#kode_pos').val(kode_pos);
        $('#telp').val(telp);
        $('#alamat').val(alamat);
        
        $('#modal_detal').modal('show');
      },
      error:function(){
        alert('error get anggota');
      }
    });
  }

function detail_pesanan2(id_transaksi_detail){
    $.ajax({
      url: '<?php echo base_url('Admin/Pemesanan/ajax_get_detail_transaksi_paket') ?>',
      type: 'POST',
      dataType: 'JSON',
      data: {
      id_transaksi_detail: id_transaksi_detail,
      },
      success:function(data){
        console.log(data);
        $('#nama_item2').val(data.nama_item);
        $('#des').val(data.deskripsi);
        $('#harga2').val('Rp. '+data.harga_modal);
        $('#tgl_upacara2').val(data.tgl_upacara);
  
        $('#modal_detal2').modal('show');
      },
      error:function(){
        alert('error get anggota');
      }
    });
  }
  

 


</script>
</body>
</html>


