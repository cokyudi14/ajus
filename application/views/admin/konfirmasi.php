
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <section class="content-header">
      <?php if ($this->session->flashdata('success')): ?>
        <div class="callout callout-success lead">
          <h4>Berhasil !</h4>
          <p><?php echo $this->session->flashdata('success')?></p>
        </div>
      <?php endif; ?>
      <h1>
        Kelola Konfirmasi

      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box box-solid box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">List Item</h3>
        </div>
        <div class="box-body">
          <table class="table table1 table-striped table-bordered table-hover" id='tb_list_item'>
            <thead>
              <tr>
                <th>No</th>
                <th>Bank Tujuan</th>
                <th>Pemilik Rekening</th>
                <th>No rekening</th>
               <!--  <th>Bukti Pembayaran</th> -->
                <th style="width: 20%">Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php $no=1; ?>
              <?php foreach ($konfirmasi as $key) { ?>
                <tr>
                  <td><?php echo $no ?></td>
                  <td><?php echo $key->bank_tujuan ?></td>
                  <td><?php echo $key->pemilik_rekening ?></td>
                  <td><?php echo $key->nomor_rekening ?></td>
                  <!-- <td><a target="_blank" href="<?php echo base_url('assets/images/bukti/'.$key->bukti_pembayaran.''); ?>">lihat</a> </td>
                  -->
                  <td class="btn-group">
                     
                    <?php if($key->status_pembayaran==1){ ?>
                          <a href="javascript:void(0)" title="Detail item pesanan" class="btn btn-primary" onclick="detail('<?php echo $key->id_transaksi ?>','<?php echo $key->id_transaksi_paket ?>','<?php echo $key->bukti_pembayaran ?>')">  Detail</a>
                          <a href="<?php echo base_url("Admin/Konfirmasi/terima/".$key->id_pembayaran."")?>" title="Terima pembayaran" class="btn btn-success"  onclick="return confirm('Yakin ingin menerima pembayaran ?')"> Terima</a>
                           <a href="<?php echo base_url("Admin/Konfirmasi/tolak/".$key->id_pembayaran."")?>" title="Tolak pembayaran" class="btn btn-danger"  onclick="return confirm('Yakin ingin menolak pembayaran ?')"> Tolak</a>
                    <?php }
                    if($key->status_pembayaran==2){ ?>
                          Sudah dikonfirmasi
                    <?php } ?>
                     
                  </td>
                </tr>
                <?php $no++; } ?>
            
            </tbody>
          </table>
        </div>
      </div>
    </section>

    <div class="modal fade" id="modal_add_item" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-md" role="document">
        <!--Content-->
        <div class="modal-content">
          <!--Header-->
          <div class="modal-header" style="background-color: #367fa9;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 id="header" class="modal-title" style="color:white"><i class="fa fa-plus"></i>    Tambah Item</h4>
          </div>

          <!--Body-->
          <div class="modal-body">
            <form id="" class="" action="<?php echo base_url('Admin/Item/add_item') ?>" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <label for="username">Nama Item</label>
                <input type="text" class="form-control" name="nama_item" placeholder="Nama Item" required>
              </div>
              <div class="form-group">
                <label for="username">Jenis Item</label>
                <input type="text" class="form-control" name="jenis_item" placeholder="Nama Item" value="<?=$jenis_item[0]->jenis_item?>" required readonly="">
              </div>
              <div class="form-group">
                <label for="username">Lama Buat</label>
                <input type="text" class="form-control" name="lama_buat" placeholder="Lama Buat" required>
              </div>
              <div class="form-group">
                <label for="username">Lama Tahan</label>
                <input type="text" class="form-control" name="lama_tahan" placeholder="Lama Tahan" required>
              </div>
              <div class="form-group">
                <label for="username">Harga Modal</label>
                <input type="text" class="form-control" name="harga_modal" placeholder="Harga Modal" required>
              </div>
              <div class="form-group">
                <label for="username">Foto Item</label>
                <input type="file" class="form-control" name="foto_item" placeholder="Foto Item" required>
              </div>
              <center><b>Preview Foto</b></center><br>
              <div align="center">
                <img id="preview_foto" width="60%" class="img-responsive" src="<?php echo base_url('assets/images/no_image_available.jpg'); ?>">
              </div><br>
              <button class="btn btn-primary btn-block" type="submit" name="button"><i class="fa fa-paper-plane-o"></i>   Kirim</button>
            </form>
          </div>
          <!--Footer-->
        </div>
        <!--/.Content-->
      </div>
    </div>

    <div class="modal fade" id="modal_edit_item" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-md" role="document">
        <!--Content-->
        <div class="modal-content">
          <!--Header-->
          <div class="modal-header" style="background-color: #367fa9;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 id="header" class="modal-title" style="color:white; text-align: center;">Detail Barang</h4>
          </div>

          <!--Body-->
          <div class="modal-body">
             <input type="text" id="harga"  hidden></input>
              <input type="text" id="harga2"  hidden></input>
             <b id="item_title">Item yang dipesan</b>
              <ol id="item"></ol>
             <b id="paket_title">Paket yang dipesan</b>
              <ol id="paket"></ol>
              <b >Total harga</b> : <b id="t_harga"></b>
              <br>
              <br>
              <br>
              <div style="text-align: center;">
                <img id="bukti" width="400px" height="500px"  >
              </div>
            
          </div>
          <!--Footer-->
        </div>
        <!--/.Content-->
      </div>
    </div>

    <div class="modal fade" id="modal_edit_harga" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-md" role="document">
        <!--Content-->
        <div class="modal-content">
          <!--Header-->
          <div class="modal-header" style="background-color: #367fa9;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 id="header" class="modal-title" style="color:white"><i class="fa fa-pencil"></i>    Edit Harga</h4>
          </div>

          <!--Body-->
          <div class="modal-body">
            <form id="" class="" action="<?php echo base_url('Admin/Item/update_harga') ?>" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <label for="username">Nama Item</label>
                <input type="text" class="form-control" id="harga_nama_item" name="nama_item" placeholder="Nama Item" readonly required>
                <input type="hidden" class="form-control" id="harga_id_item" name="id_item" required>
              </div>
              <div class="form-group">
                <label for="username">Harga Modal</label>
                <input type="text" class="form-control" id="harga_harga_modal" name="harga_modal" placeholder="Harga Modal" readonly required>
              </div>
              <div class="form-group">
                <label for="username">Harga Jual</label>
                <input type="text" min="" class="form-control" id="harga_harga_jual" name="harga_jual" placeholder="Harga Jual" required>
              </div>
              <button class="btn btn-primary btn-block" type="submit" name="button"><i class="fa fa-paper-plane-o"></i>   Kirim</button>
            </form>
          </div>
          <!--Footer-->
        </div>
        <!--/.Content-->
      </div>
    </div>


  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; 2018</strong> All rights
    reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('bower_components/jquery/dist/jquery.min.js') ?>"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url('bower_components/jquery-ui/jquery-ui.min.js') ?>"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('bower_components/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
<!-- Morris.js charts -->
<script src="<?php echo base_url('bower_components/raphael/raphael.min.js') ?>"></script>
<script src="<?php echo base_url('bower_components/morris.js/morris.min.js') ?>"></script>
<!-- Sparkline -->
<script src="<?php echo base_url('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') ?>"></script>
<!-- jvectormap -->
<script src="<?php echo base_url('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') ?>"></script>
<script src="<?php echo base_url('plugins/jvectormap/jquery-jvectormap-world-mill-en.js') ?>"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url('bower_components/jquery-knob/dist/jquery.knob.min.js') ?>"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url('bower_components/moment/min/moment.min.js') ?>"></script>
<script src="<?php echo base_url('bower_components/bootstrap-daterangepicker/daterangepicker.js') ?>"></script>
<!-- datepicker -->
<script src="<?php echo base_url('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') ?>"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') ?>"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url('bower_components/jquery-slimscroll/jquery.slimscroll.min.js') ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('bower_components/fastclick/lib/fastclick.js') ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('dist/js/adminlte.min.js') ?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url('dist/js/pages/dashboard.js') ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('dist/js/demo.js') ?>"></script>
<script src="<?php echo base_url('bower_components/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo base_url('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>

<script>
  $.widget.bridge('uibutton', $.ui.button);
  $(document).ready(function() {
    $('#btn_konfirmasi').addClass('active');
    $('#tb_list_item').DataTable();
    $('#tittle').text('SIM | List Konfirmasi');
  });

  $('#btn_add_item').click(function() {
    $('#modal_add_item').modal('show');
  });

  

   function detail(id_transaksi,id_transaksi_paket,bukti_pembayaran){
        $("ol").empty();
        $("#harga").val('0');
        $("#harga2").val('0');
        $("#t_harga").empty();
        // var harga_paket = 0;
    $.ajax({
      url: '<?php echo base_url('Admin/Konfirmasi/get_detail_pembayaran2') ?>',
      type: 'POST',
      dataType: 'JSON',
      data: {
      id_transaksi: id_transaksi, id_transaksi_paket: id_transaksi_paket,
      },
      success:function(data_paket){
       
         $('#paket_title').css('display','block');
        // for (i = 0; i < data.length; i++) { 
        //       $("#item").append("<li>"+data[i].nama_item+"</li>");
        //   }
        if (data_paket.length==0) { 
         $('#paket_title').css('display','none');
         }
          for (j = 0; j < data_paket.length; j++) { 
              $("#paket").append("<li>"+data_paket[j].nama_paket+"</li>");
          }
        $("#harga").val(data_paket[0].harga_paket);
        harga_paket = data_paket[0].harga_paket;
         console.log(data_paket);
    
       
    
      },
      error:function(){
        alert(id_transaksi_paket);
      }
    });

    $.ajax({
      url: '<?php echo base_url('Admin/Konfirmasi/get_detail_pembayaran') ?>',
      type: 'POST',
      dataType: 'JSON',
      data: {
      id_transaksi: id_transaksi, id_transaksi_paket: id_transaksi_paket,
      },
      success:function(data){
       $('#item_title').css('display','block');
      
        for (i = 0; i < data.length; i++) { 
              $("#item").append("<li>"+data[i].nama_item+"  <b>( "+data[i].qty_item+" buah )</b></li>");
              //t_harga_item = parseInt(t_harga_item)+parseInt(data[i].harga);

          }
          // for (j = 0; j < data_paket.length; j++) { 
          //     $("#paket").append("<li>"+data_paket[j].nama_paket+"</li>");
          // }
           if (data.length==0) { 
         $('#item_title').css('display','none');
        }
         console.log(data);
          if (data.length!=0) { 
         $("#harga2").val(data[0].total_harga);
        }
         


        
         // //alert(t_harga);
         // $("#t_harga").append('Rp. '+t_harga);
       
        $('#modal_edit_item').modal('show');
      },
      error:function(){
        alert(id_transaksi_paket);
      }
    });

    
     setTimeout(function() {
      var harga_paket = $('#harga').val();
     var harga_item = $('#harga2').val();
     t_harga = parseInt(harga_paket)+parseInt(harga_item);

      //format rupiah
          var bilangan = t_harga;
            
          var number_string = bilangan.toString(),
              sisa    = number_string.length % 3,
              rupiah  = number_string.substr(0, sisa),
              ribuan  = number_string.substr(sisa).match(/\d{3}/g);
              
          if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
          }
     $("#t_harga").append('Rp. '+rupiah);
      }, 1000);

    
    $('#bukti').attr('src', '<?= base_url() ?>assets/images/bukti/'+bukti_pembayaran)

  }


</script>
</body>
</html>


