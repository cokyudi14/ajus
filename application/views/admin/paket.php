
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <section class="content-header">
      <?php if ($this->session->flashdata('success')): ?>
        <div class="callout callout-success lead">
          <h4>Berhasil !</h4>
          <p><?php echo $this->session->flashdata('success')?></p>
        </div>
      <?php endif; ?>
      <h1>
        Kelola Paket
   <button class="pull-right btn btn-success" type="button" name="btn_add_paket" id='btn_add_paket'><i class="fa fa-plus"></i>   Tambah Paket</button>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div id="tabel_paket" class="box box-solid box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">List Item</h3>
        </div>
        <div class="box-body">
          <table class="table table1 table-striped table-bordered table-hover" id='tb_list_item'>
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Paket</th>
                <th>Upacara</th>
                <th>Harga Paket</th>
                <!-- <th>Lama Buat Paket</th> -->
                <th>Foto</th>
                <th style="text-align: center; width: 25%">Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php $no=1; ?>
              <?php foreach ($paket as $key) {?>
                <tr>
                  <td><?php echo $no ?></td>
                  <td><?php echo $key->nama_paket ?></td>
                  <td><?php echo $key->nama_upacara ?></td>
                  <td>Rp. <?php echo number_format($key->harga_paket,0,".",".")  ?></td>
                 <!--  <td><?php echo $key->lama_buat_paket ?> Hari</td> -->
                  <td><?php echo $key->foto_paket ?> </td>
                  <td class="btn-group">
                    <a  href="<?php echo base_url("Admin/Paket/detail_paket/".$key->id_paket."")?>" title="detail item" class="btn btn-primary"><i class="fa fa-eye"></i>  detail item </a>
                    <a href="javascript:void(0)" title="Edit member" class="btn btn-warning" onclick="edit_paket('<?php echo $key->id_paket ?>')"><i class="fa fa-pencil"></i>  Edit </a>
                    <a href="<?php echo base_url("Admin/Paket/delete_paket/".$key->id_paket."")?>" title="Hapus member" onclick="return confirm('Yakin ingin menghapus data?')" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i>   Hapus</a>
                  </td>
                </tr>
                <?php $no++; ?>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
      <div id="detail_paket">
        
      </div>
    </section>

    <div class="modal fade" id="modal_add_paket" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-md" role="document">
        <!--Content-->
        <div class="modal-content">
          <!--Header-->
          <div class="modal-header" style="background-color: #367fa9;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 id="header" class="modal-title" style="color:white"><i class="fa fa-plus"></i>    Tambah Paket</h4>
          </div>

          <!--Body-->
          <div class="modal-body">
            <form id="" class="" action="<?php echo base_url('Admin/Paket/add_paket') ?>" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <label for="username">Nama Paket</label>
                <input type="text" class="form-control" name="nama_paket" placeholder="Nama Paket" required>
              </div>
              <div class="form-group">
                <label for="username">Upacara</label>
                 <select name="upacara_id"  class="form-control"  >
                   <?php foreach ($upacara as $data) {
                      echo ' <option value="'.$data->id_upacara.'"> '.$data->nama_upacara.' </option>  ';
                   }
                   ?>

                                          
                 </select>
              </div>
              <div class="form-group">
                <label for="username">Harga paket</label>
                <input type="text" class="form-control" name="harga_paket" placeholder="Harga Paket" required>
              </div>
              <div class="form-group">
                <label for="username">Deskripsi</label>
                <textarea class="form-control" name="deskripsi" placeholder="Deskripsi" required></textarea>
              </div>
             <!--  <div class="form-group">
                <label for="username">Lama buat paket</label>
                <input type="text" class="form-control" name="lama_buat_paket" placeholder="Lama Buat" required>
              </div> -->
              <div class="form-group">
                <label for="username">Foto Item</label>
                <input type="file" class="form-control" name="foto_paket" placeholder="Foto Paket" required>
              </div>
              <center><b>Preview Foto</b></center><br>
              <div align="center">
                <img id="preview_foto" width="60%" class="img-responsive" src="<?php echo base_url('assets/images/no_image_available.jpg'); ?>">
              </div><br>
              <button class="btn btn-primary btn-block" type="submit" name="button">simpan</button>
            </form>
          </div>
          <!--Footer-->
        </div>
        <!--/.Content-->
      </div>
    </div>

     <div class="modal fade" id="modal_edit_paket" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-md" role="document">
        <!--Content-->
        <div class="modal-content">
          <!--Header-->
          <div class="modal-header" style="background-color: #367fa9;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 id="header" class="modal-title" style="color:white"> Edit Paket</h4>
          </div>

          <!--Body-->
          <div class="modal-body">
            <form id="" class="" action="<?php echo base_url('Admin/Paket/update_paket') ?>" method="post" enctype="multipart/form-data">
              <input type="hidden" class="form-control" name="edit_id_paket" id="edit_id_paket" >
              <div class="form-group">
                <label for="username">Nama Paket</label>
                <input type="text" class="form-control" name="edit_nama_paket" id="edit_nama_paket" placeholder="Nama Paket" required>
              </div>
              <div class="form-group">
                <label for="username">Upacara</label>
                 <select name="edit_nama_upacara"  class="form-control"  >
                  <option  id="edit_nama_upacara"></option> 
                   <?php foreach ($upacara as $data) {
                      echo ' <option value="'.$data->id_upacara.'"> '.$data->nama_upacara.' </option>  ';
                   }
                   ?>

                                          
                 </select>
              </div>
              <div class="form-group">
                <label for="username">Harga Paket</label>
                <input type="text" class="form-control" name="edit_harga_paket" id="edit_harga_paket" placeholder="Harga Paket" required>
              </div>
              <div class="form-group">
                <label for="username">Deskripsi</label>
                <textarea class="form-control" name="edit_deskripsi" id="edit_deskripsi" placeholder="Deskripsi" required></textarea>
              </div>
             <!--  <div class="form-group">
                <label for="username">Lama buat paket</label>
                <input type="text" class="form-control" name="edit_lama_buat_paket" id="edit_lama_buat_paket" placeholder="Lama Buat" required>
              </div> -->
              <div class="form-group">
                <label for="username">Foto Item</label>
                <input type="file" class="form-control" name="edit_foto_paket" placeholder="Foto Paket" required>
              </div>
              <center><b>Preview Foto</b></center><br>
              <div align="center">
                <img id="edit_preview_foto" width="60%" class="img-responsive" src="<?php echo base_url('assets/images/no_image_available.jpg'); ?>">
              </div><br>
              <button class="btn btn-primary btn-block" type="submit" name="button">simpan</button>
            </form>
          </div>
          <!--Footer-->
        </div>
        <!--/.Content-->
      </div>
    </div>

     <div class="modal fade" id="modal_tambah_item_paket" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-md" role="document">
        <!--Content-->
        <div class="modal-content">
          <!--Header-->
          <div class="modal-header" style="background-color: #367fa9;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 id="header" class="modal-title" style="color:white"> Tambah Item Paket</h4>
          </div>

          <!--Body-->
          <div class="modal-body">
            <form id="" class="" action="<?php echo base_url('Admin/Paket/add_paket') ?>" method="post" enctype="multipart/form-data">
              <input type="hidden" class="form-control" name="id_item_paket" id="id_item_paket" >
              <div class="form-group">
                <label for="username">Item</label>
                 <select name="upacara_id"  class="form-control"  >
                   <?php foreach ($item as $data) {
                      echo ' <option value="'.$data->id_item.'"> '.$data->nama_item.' </option>  ';
                   }
                   ?>

                                          
                 </select>
              </div>
              <div class="form-group">
                <label for="username">Jumlah</label>
                <input type="text" class="form-control" name="harga_paket" placeholder="Jumlah item" required>
              </div>
              <button class="btn btn-primary btn-block" type="submit" name="button">simpan</button>
            </form>
          </div>
          <!--Footer-->
        </div>
        <!--/.Content-->
      </div>
    </div>

   

    

  </div>
  <!-- /.content-wrapper -->
   <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; 2018</strong> All rights
    reserved.
  </footer> 
  
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('bower_components/jquery/dist/jquery.min.js') ?>"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url('bower_components/jquery-ui/jquery-ui.min.js') ?>"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('bower_components/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
<!-- Morris.js charts -->
<script src="<?php echo base_url('bower_components/raphael/raphael.min.js') ?>"></script>
<script src="<?php echo base_url('bower_components/morris.js/morris.min.js') ?>"></script>
<!-- Sparkline -->
<script src="<?php echo base_url('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') ?>"></script>
<!-- jvectormap -->
<script src="<?php echo base_url('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') ?>"></script>
<script src="<?php echo base_url('plugins/jvectormap/jquery-jvectormap-world-mill-en.js') ?>"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url('bower_components/jquery-knob/dist/jquery.knob.min.js') ?>"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url('bower_components/moment/min/moment.min.js') ?>"></script>
<script src="<?php echo base_url('bower_components/bootstrap-daterangepicker/daterangepicker.js') ?>"></script>
<!-- datepicker -->
<script src="<?php echo base_url('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') ?>"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') ?>"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url('bower_components/jquery-slimscroll/jquery.slimscroll.min.js') ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('bower_components/fastclick/lib/fastclick.js') ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('dist/js/adminlte.min.js') ?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url('dist/js/pages/dashboard.js') ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('dist/js/demo.js') ?>"></script>
<script src="<?php echo base_url('bower_components/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo base_url('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>

<script>
  $.widget.bridge('uibutton', $.ui.button);
  $(document).ready(function() {
    $('#btn_kelola_paket').addClass('active');
    $('#tb_list_item').DataTable();
    $('#tittle').text('SIM | Kelola Paket');
  });

  $('#btn_add_paket').click(function() {
    $('#modal_add_paket').modal('show');
  });

  function edit_paket(id_paket){
    $.ajax({
      url: '<?php echo base_url('Admin/Paket/ajax_get_paket') ?>',
      type: 'POST',
      dataType: 'JSON',
      data: {
      id_paket: id_paket,
      },
      success:function(data){
        console.log(data);
        $('#edit_id_paket').val(data.id_paket);
        $('#edit_nama_paket').val(data.nama_paket);
        $('#edit_nama_upacara').val(data.upacara_id);
        $('#edit_nama_upacara').html(data.nama_upacara);
        $('#edit_lama_buat_paket').val(data.lama_buat_paket);
        $('#edit_harga_paket').val(data.harga_paket);
        $('#edit_deskripsi').val(data.deskripsi);
        $('#modal_edit_paket').modal('show');
      },
      error:function(){
        alert('error get anggota');
      }
    });
  }

   function tambah_item_paket(id_paket){
     $('#id_item_paket').val(id_paket);
        $('#modal_tambah_item_paket').modal('show');
  }

  function detail_paket(id_paket)
      {
        $("#tabel_paket").css('display','none'); 
        $("#btn_add_paket").css('display','none'); 
        
       $.ajax({
         url:"<?php echo base_url();?>Admin/Paket/detail_paket/"+id_paket+"",
         success: function(response){
         $("#detail_paket").html(response);
         },
         dataType:"html"
       });

       return false;
  }

  $('input[name=foto_paket]').change(function() {
    readURL(this);
  });

  $('input[name=edit_foto_paket]').change(function() {
    editReadURL(this);
  });

  function readURL(input) {
    var reader = new FileReader();
    if (input.files && input.files[0]) {
      reader.onload = function(e) {
        if(input.name=='foto_paket'){
          $('#preview_foto').attr('src', e.target.result);
        }
      }
      reader.readAsDataURL(input.files[0]);
    }
    else{
      if(input.name=='foto_paket'){
        var img = '<img id="preview_foto" width="60%" class="img-responsive" src="<?php echo base_url('assets/images/no_image_available.jpg'); ?>">';
        $('#preview_foto').replaceWith(img);
      }
    }
  }

  function editReadURL(input) {
    var reader = new FileReader();
    if (input.files && input.files[0]) {
      reader.onload = function(e) {
        if(input.name=='edit_foto_paket'){
          $('#edit_preview_foto').attr('src', e.target.result);
        }
      }
      reader.readAsDataURL(input.files[0]);
    }
    else{
      if(input.name=='edit_foto_paket'){
        var img = '<img id="edit_preview_foto" width="60%" class="img-responsive" src="<?php echo base_url('assets/images/no_image_available.jpg'); ?>">';
        $('#preview_foto').replaceWith(img);
      }
    }
  }
</script>
</body>
</html>


