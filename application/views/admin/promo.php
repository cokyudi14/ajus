
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <section class="content-header">
      <?php 
       $tab1='active';
       $tab2='';
       $tab11='active';
       $tab22='';
      if ($this->session->flashdata('item')): ?>
        <div class="callout callout-success lead">
          <h4>Berhasil !</h4>
          <p><?php echo $this->session->flashdata('item')?></p>
        </div>
      <?php 
       $tab1='';
       $tab2='active';
        $tab11='';
       $tab22='active';
      endif; 

      if ($this->session->flashdata('paket')): ?>
        <div class="callout callout-success lead">
          <h4>Berhasil !</h4>
          <p><?php echo $this->session->flashdata('paket')?></p>
        </div>
      <?php 
      endif;?>
      <h1>
        Kelola Promo
  
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="<?php echo $tab11 ?>"><a href="#tab_1" data-toggle="tab" aria-expanded="true"> Paket Promo</a></li>
              <li class="<?php echo $tab22 ?>"><a href="#tab_2" data-toggle="tab" aria-expanded="false">Item Promo</a></li>
            </ul>
            <div class="tab-content">

              <div class="tab-pane <?php echo $tab1 ?>" id="tab_1">
                
               <div class="box-body">
                 <button class="pull-right btn btn-primary" type="button" name="btn_add_promo" id='btn_add_promo'>Tambah Paket Promo</button>
               </div>
                
                 <div id="tabel_paket" class="box box-solid box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title">List Paket Promo</h3>
                    </div>
                    <div class="box-body">
                      <table class="table table1 table-striped table-bordered table-hover" id='tb_list_paket'>
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Nama Paket</th>
                            <th>Upacara</th>
                            <th>Harga Promo</th>
                   
                            <th style="text-align: center;width: 24%">Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $no=1; ?>
                          <?php foreach ($paket as $key) {?>
                            <tr>
                              <td><?php echo $no ?></td>
                              <td><?php echo $key->nama_paket ?></td>
                              <td><?php echo $key->nama_upacara ?></td>
                              <td>Rp. <?php echo number_format($key->harga_paket,0,".",".")  ?></td>
                           
                              <td class="btn-group">
                                <a  href="<?php echo base_url("Admin/Paket/detail_paket/".$key->id_paket."")?>" title="detail item" class="btn btn-success"><i class="fa fa-eye"></i>  detail item </a>
                                <a href="javascript:void(0)" title="Edit member" class="btn btn-danger" onclick="hapus_promo_paket('<?php echo $key->id_paket ?>')"><i class="fa fa-trash" aria-hidden="true"></i>  Hapus Promo </a>
                               
                              </td>
                            </tr>
                            <?php $no++; ?>
                          <?php } ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane <?php echo $tab2 ?>" id="tab_2">

               <div class="box-body">
                 <button class="pull-right btn btn-success" type="button" id='btn_add_promo_item'>Tambah Promo Item</button>
               </div>
                
                 <div id="tabel_paket" class="box box-solid box-success">
                    <div class="box-header with-border">
                      <h3 class="box-title">List Promo Item</h3>
                    </div>
                    <div class="box-body">
                     <table class="table table1 table-striped table-bordered table-hover" id='tb_list_item'>
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Nama Item</th>
                            <th>Jenis Item</th>
                            <th>Supplier</th>
                            <th>Lama Buat</th>
                            <th>Lama Tahan</th>
                            <th>Harga Promo</th>
                            <th>Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $no=1; ?>
                          <?php foreach ($item as $key) {?>
                            <tr>
                              <td><?php echo $no ?></td>
                              <td><?php echo $key->nama_item ?></td>
                              <td><?php echo $key->jenis_item ?></td>
                              <td><?php echo $key->nama ?></td>
                              <td><?php echo $key->lama_buat ?> Hari</td>
                              <td><?php echo $key->lama_tahan ?> Hari</td>
                              <td>Rp. <?php echo number_format($key->harga_jual,0,".",".")  ?></td>
                              <td class="btn-group">
                                <a href="javascript:void(0)" title="Edit member" class="btn btn-danger" onclick="hapus_promo_item('<?php echo $key->id_item ?>')"><i class="fa fa-trash" aria-hidden="true"></i>  Hapus Promo </a>
                              </td>
                            </tr>
                            <?php $no++; ?>
                          <?php } ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
               
              </div>
            </div>
            <!-- /.tab-content -->
          </div>
     
     
    </section>

    <div class="modal fade" id="modal_add_promo" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-md" role="document">
        <!--Content-->
        <div class="modal-content">
          <!--Header-->
          <div class="modal-header" style="background-color: #367fa9;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 id="header" class="modal-title" style="color:white"> Tambah Paket Promo</h4>
          </div>

          <!--Body-->
          <div class="modal-body">
            <form id="" class="" action="<?php echo base_url('Admin/Promo/add_promo') ?>" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <label for="username">Paket</label>
                 <select name="id_paket"  class="form-control"  >
                   <?php foreach ($paket2 as $data) {
                      echo ' <option value="'.$data->id_paket.'"> '.$data->nama_paket.' </option>  ';
                   }
                   ?>                         
                 </select>
              </div>
              <div class="form-group">
                <label for="username">Harga Promo</label>
                <input type="text" class="form-control" name="harga_paket" placeholder="Harga Promo" required>
              </div>
              <button class="btn btn-primary btn-block" type="submit" name="button">simpan</button>
            </form>
          </div>

          <!--Footer-->
        </div>
        <!--/.Content-->
      </div>
    </div>

     <div class="modal fade" id="modal_hapus_paket_promo" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-md" role="document">
        <!--Content-->
         <div class="modal-content">
          <!--Header-->
          <div class="modal-header" style="background-color: #367fa9;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 id="header" class="modal-title" style="color:white"> Hapus Paket Promo</h4>
          </div>

          <!--Body-->
          <div class="modal-body">
            <form id="" class="" action="<?php echo base_url('Admin/Promo/delete_promo') ?>" method="post" enctype="multipart/form-data">
              <input type="hidden" class="form-control" id="hp_id_paket" name="id_paket" placeholder="Harga Promo" readonly>
              <div class="form-group">
                <label for="username">Paket</label>
                 <input type="text" class="form-control" id="hp_nama_paket" name="nama_paket" placeholder="Harga Promo" readonly>
              </div>
              <div class="form-group">
                <label for="username">Harga Normal</label>
                <input type="text" class="form-control" name="harga_paket" placeholder="Harga Normal" required>
              </div>
              <button class="btn btn-primary btn-block" type="submit" name="button">simpan</button>
            </form>
          </div>

          <!--Footer-->
        </div>
        <!--/.Content-->
      </div>
    </div>

    <div class="modal fade" id="modal_add_promo_item" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-md" role="document">
        <!--Content-->
        <div class="modal-content">
          <!--Header-->
          <div class="modal-header" style="background-color: #00a65a;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 id="header" class="modal-title" style="color:white"> Tambah Item Promo</h4>
          </div>

          <!--Body-->
          <div class="modal-body">
            <form id="" class="" action="<?php echo base_url('Admin/Promo/add_promo_item') ?>" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <label for="username">Item</label>
                 <select name="id_item"  class="form-control"  >
                   <?php foreach ($item2 as $data) {
                      echo ' <option value="'.$data->id_item.'"> '.$data->nama_item.' </option>  ';
                   }
                   ?>                         
                 </select>
              </div>
              <div class="form-group">
                <label for="username">Harga Promo</label>
                <input type="text" class="form-control" name="harga_item" placeholder="Harga Promo" required>
              </div>
              <button class="btn btn-success btn-block" type="submit" name="button">simpan</button>
            </form>
          </div>

          <!--Footer-->
        </div>
        <!--/.Content-->
      </div>
    </div>

    <div class="modal fade" id="modal_hapus_item_promo" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-md" role="document">
        <!--Content-->
         <div class="modal-content">
          <!--Header-->
          <div class="modal-header" style="background-color: #367fa9;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 id="header" class="modal-title" style="color:white"> Hapus Item Promo</h4>
          </div>

          <!--Body-->
          <div class="modal-body">
            <form id="" class="" action="<?php echo base_url('Admin/Promo/delete_promo_item') ?>" method="post" enctype="multipart/form-data">
              <input type="hidden" class="form-control" id="hp_id_item" name="id_item" placeholder="Harga Promo" readonly>
              <div class="form-group">
                <label for="username">Paket</label>
                 <input type="text" class="form-control" id="hp_nama_item" name="nama_item" placeholder="Harga Promo" readonly>
              </div>
              <div class="form-group">
                <label for="username">Harga Normal</label>
                <input type="text" class="form-control" name="harga_item" placeholder="Harga Normal" required>
              </div>
              <button class="btn btn-primary btn-block" type="submit" name="button">simpan</button>
            </form>
          </div>

          <!--Footer-->
        </div>
        <!--/.Content-->
      </div>
    </div>

     
   

    

  </div>
  <!-- /.content-wrapper -->
   <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; 2018</strong> All rights
    reserved.
  </footer>
  
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('bower_components/jquery/dist/jquery.min.js') ?>"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url('bower_components/jquery-ui/jquery-ui.min.js') ?>"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('bower_components/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
<!-- Morris.js charts -->
<script src="<?php echo base_url('bower_components/raphael/raphael.min.js') ?>"></script>
<script src="<?php echo base_url('bower_components/morris.js/morris.min.js') ?>"></script>
<!-- Sparkline -->
<script src="<?php echo base_url('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') ?>"></script>
<!-- jvectormap -->
<script src="<?php echo base_url('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') ?>"></script>
<script src="<?php echo base_url('plugins/jvectormap/jquery-jvectormap-world-mill-en.js') ?>"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url('bower_components/jquery-knob/dist/jquery.knob.min.js') ?>"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url('bower_components/moment/min/moment.min.js') ?>"></script>
<script src="<?php echo base_url('bower_components/bootstrap-daterangepicker/daterangepicker.js') ?>"></script>
<!-- datepicker -->
<script src="<?php echo base_url('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') ?>"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') ?>"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url('bower_components/jquery-slimscroll/jquery.slimscroll.min.js') ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('bower_components/fastclick/lib/fastclick.js') ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('dist/js/adminlte.min.js') ?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url('dist/js/pages/dashboard.js') ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('dist/js/demo.js') ?>"></script>
<script src="<?php echo base_url('bower_components/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo base_url('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>

<script>
  $.widget.bridge('uibutton', $.ui.button);
  $(document).ready(function() {
    $('#btn_kelola_promo').addClass('active');
    $('#tb_list_paket').DataTable();
    $('#tb_list_item').DataTable();
    $('#tittle').text('SIM | Kelola Promo');
  });

  $('#btn_add_promo').click(function() {
    $('#modal_add_promo').modal('show');
  });

  $('#btn_add_promo_item').click(function() {
    $('#modal_add_promo_item').modal('show');
  });

  function hapus_promo_paket(id_paket){
    $.ajax({
      url: '<?php echo base_url('Admin/Paket/ajax_get_paket') ?>',
      type: 'POST',
      dataType: 'JSON',
      data: {
      id_paket: id_paket,
      },
      success:function(data){
        console.log(data);
        $('#hp_id_paket').val(data.id_paket);
        $('#hp_nama_paket').val(data.nama_paket);
        $('#modal_hapus_paket_promo').modal('show');
      },
      error:function(){
        alert('error get anggota');
      }
    });
  }

   function hapus_promo_item(id_item){
    $.ajax({
      url: '<?php echo base_url('Admin/Item/ajax_get_item') ?>',
      type: 'POST',
      dataType: 'JSON',
      data: {
      id_item: id_item,
      },
      success:function(data){
        console.log(data);
        $('#hp_id_item').val(data.id_item);
        $('#hp_nama_item').val(data.nama_item);
        $('#modal_hapus_item_promo').modal('show');
      },
      error:function(){
        alert('error get anggota');
      }
    });
  }

   function tambah_item_paket(id_paket){
     $('#id_item_paket').val(id_paket);
        $('#modal_tambah_item_paket').modal('show');
  }

  function detail_paket(id_paket)
      {
        $("#tabel_paket").css('display','none'); 
        $("#btn_add_paket").css('display','none'); 
        
       $.ajax({
         url:"<?php echo base_url();?>Admin/Paket/detail_paket/"+id_paket+"",
         success: function(response){
         $("#detail_paket").html(response);
         },
         dataType:"html"
       });

       return false;
  }


</script>
</body>
</html>


