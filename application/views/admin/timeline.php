<div class="content-wrapper">
  <section class="content-header">
    <h1>ID Transaksi: <?= $id ?></h1>
  </section>

  <section class="content">
    <a href="<?= site_url('Admin/Transaksi/schedule/'.$id) ?>" class="btn btn-primary">Buat Schedule</a><br /><br />
    <?php if (sizeof($schedule_pesan) > 0): ?>
      <h3>PESAN</h3>
    <?php endif ?>
    <ul class="timeline">
      <?php for ($key=0; $key < sizeof($schedule_pesan); $key++): ?>
        <li>
          <i class="fa fa-shopping-cart bg-blue"></i>
          <div class="timeline-item">
            <h3 class="timeline-header"><a href="#"><?= date_format(date_create($schedule_pesan[$key]->tgl_pemesanan), 'd F Y') ?></a></h3>

            <div class="timeline-body">
              <ul>
                <li><?= $schedule_pesan[$key]->nama_item ?></li>
                <?php if (isset($schedule_pesan[$key+1]->tgl_pemesanan)): ?>

                  <?php while ($schedule_pesan[$key]->tgl_pemesanan == $schedule_pesan[$key+1]->tgl_pemesanan) { ?>
                    <li><?= $schedule_pesan[$key+1]->nama_item ?></li>
                  <?php
                    $key++;
                    if (empty($schedule_pesan[$key+1]->tgl_pemesanan)) break;
                  } ?>

                <?php endif; ?>
              </ul>
            </div>
          </div>
        </li>
      <?php endfor; ?>
    </ul>
    
    <?php if (sizeof($schedule_ambil) > 0): ?>
      <h3>AMBIL PESANAN</h3>
    <?php endif ?>
    <ul class="timeline">
      <?php for ($key=0; $key < sizeof($schedule_ambil); $key++): ?>
        <li>
          <i class="fa fa-shopping-cart bg-blue"></i>
          <div class="timeline-item">
            <h3 class="timeline-header"><a href="#"><?= date_format(date_create($schedule_ambil[$key]->tgl_pemesanan), 'd F Y') ?></a></h3>

            <div class="timeline-body">
              <ul>
                <li><?= $schedule_ambil[$key]->nama_item ?></li>
                <?php if (isset($schedule_ambil[$key+1]->tgl_pemesanan)): ?>

                  <?php while ($schedule_ambil[$key]->tgl_pemesanan == $schedule_ambil[$key+1]->tgl_pemesanan) { ?>
                    <li><?= $schedule_ambil[$key+1]->nama_item ?></li>
                  <?php
                    $key++;
                    if (empty($schedule_ambil[$key+1]->tgl_pemesanan)) break;
                  } ?>

                <?php endif; ?>
              </ul>
            </div>
          </div>
        </li>
      <?php endfor; ?>
    </ul>

  </section>



</div>
<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; 2018</strong> All rights
    reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('bower_components/jquery/dist/jquery.min.js') ?>"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url('bower_components/jquery-ui/jquery-ui.min.js') ?>"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('bower_components/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
<!-- Morris.js charts -->
<script src="<?php echo base_url('bower_components/raphael/raphael.min.js') ?>"></script>
<script src="<?php echo base_url('bower_components/morris.js/morris.min.js') ?>"></script>
<!-- Sparkline -->
<script src="<?php echo base_url('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') ?>"></script>
<!-- jvectormap -->
<script src="<?php echo base_url('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') ?>"></script>
<script src="<?php echo base_url('plugins/jvectormap/jquery-jvectormap-world-mill-en.js') ?>"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url('bower_components/jquery-knob/dist/jquery.knob.min.js') ?>"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url('bower_components/moment/min/moment.min.js') ?>"></script>
<script src="<?php echo base_url('bower_components/bootstrap-daterangepicker/daterangepicker.js') ?>"></script>
<!-- datepicker -->
<script src="<?php echo base_url('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') ?>"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') ?>"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url('bower_components/jquery-slimscroll/jquery.slimscroll.min.js') ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('bower_components/fastclick/lib/fastclick.js') ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('dist/js/adminlte.min.js') ?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url('dist/js/pages/dashboard.js') ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('dist/js/demo.js') ?>"></script>
<script src="<?php echo base_url('bower_components/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo base_url('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>

</body>
</html>
