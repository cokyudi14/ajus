<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Item extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('M_user','M_supplier','M_admin'));
    $this->load->library(array('upload'));
    $this->load->helper(array('url'));
    if(!$this->session->userdata('admin_username')){
          redirect('admin/login');
    }
  }

  function index(){
    $role = $this->session->userdata('admin_role');
    $id_jenis = $this->session->userdata('jenis_item'); 
    $id_supp = $this->session->userdata('id');
    $data['jenis_item'] = $this->M_admin->ajax_get_jenis_item($id_jenis);
    if($role=='supplier'){
      $data['item'] = $this->M_admin->get_supp_item($id_supp)->result();
      $header['notif']= $this->M_admin->get_notif($id_supp)->result();
      $header['jum_notif']=count($header['notif']);
    }else{
      $data['item'] = $this->M_admin->get_all_item('tidak promo')->result();
      $header['notif']= '';
      $header['jum_notif']='';
    }


    $this->load->view('admin/head',$header);
    $this->load->view('admin/item',$data);
  }

  function ajax_get_item(){
    $id_item = $this->input->post('id_item');
    $data = $this->M_admin->ajax_get_item($id_item);
    echo json_encode($data[0]);
  }

  function ajax_gambar_item($id_item){
    $data['row'] = $this->M_admin->ajax_get_item($id_item);
    $this->load->view('admin/foto_item',$data);
  }

  function add_item(){
    $tmp = explode(".", $_FILES['foto_item']['name']);
    $ext = end($tmp);
    $gambar=url_title($this->input->post('nama_item')).'.'.$ext;
    $config['upload_path']   = './assets/images/item/';
    $config['allowed_types'] = 'gif|jpg|png|jpeg';
    $config['max_size']      = 10000000;
    $config['max_width']     = 2000;
    $config['max_height']    = 2000;
    $config['file_name']   = $gambar;
    $config['overwrite'] = TRUE;
  
    $this->upload->initialize($config);
    $upload=$this->upload->do_upload('foto_item');

    $add = [
      'nama_item'=>$this->input->post('nama_item'),
      'jenis_item'=>$this->session->userdata('jenis_item'),
      'id_supplier'=>$this->session->userdata('id'),
      'lama_buat'=>$this->input->post('lama_buat'),
      'jenis_transaksi'=>$this->input->post('jenis_transaksi'),
      'lama_tahan'=>$this->input->post('lama_tahan'),
      'harga_modal'=>$this->input->post('harga_modal'),
      'harga_jual'=>$this->input->post('harga_modal'),
      'deskripsi'=>$this->input->post('deskripsi'),
      'item_status'=>'tertunda',
      'foto_item'=>$gambar
    ];
    $data = $this->M_admin->add_item($add);

    if ($data=='berhasil') {
      $this->session->set_flashdata('success', 'Item baru telah ditambahkan!');
    }
    redirect("Admin/Item");
  }

  function update_item(){
    $id_item = $this->input->post('id_item');
    $tmp = explode(".", $_FILES['edit_foto_item']['name']);
    $ext = end($tmp);
    $gambar=url_title($this->input->post('nama_item')).'.'.$ext;
    $config['upload_path']   = './assets/images/item/';
    $config['allowed_types'] = 'gif|jpg|png|jpeg';
    $config['max_size']      = 10000000;
    $config['max_width']     = 2000;
    $config['max_height']    = 2000;
    $config['file_name']   = $gambar;
    $config['overwrite'] = TRUE;
  
    $this->upload->initialize($config);
    $upload=$this->upload->do_upload('edit_foto_item');

    $update = [
      'nama_item'=>$this->input->post('nama_item'),
      'jenis_item'=>$this->session->userdata('jenis_item'),
      'id_supplier'=>$this->session->userdata('id'),
      'lama_buat'=>$this->input->post('lama_buat'),
      'lama_tahan'=>$this->input->post('lama_tahan'),
      'jenis_transaksi'=>$this->input->post('jenis_transaksi'),
      'harga_modal'=>$this->input->post('harga_modal'),
      'harga_jual'=>$this->input->post('harga_modal'),
      'deskripsi'=>$this->input->post('deskripsi'),
      'foto_item'=>$gambar
    ];

    $data = $this->M_admin->edit_item($update,$id_item);
    if ($data=='berhasil') {
      $this->session->set_flashdata('success', 'Item telah di Update!');
    }
    else {
      $this->session->set_flashdata('error', 'Gagal mengedit item!');
    }
    redirect("Admin/Item");
  }

  function update_harga(){
    $id_item = $this->input->post('id_item');
    $update = [
      'harga_jual'=>$this->input->post('harga_jual')
    ];

    $data = $this->M_admin->edit_item($update,$id_item);
    if ($data=='berhasil') {
      $this->session->set_flashdata('success', 'Item telah di Update!');
    }
    else {
      $this->session->set_flashdata('error', 'Gagal mengedit item!');
    }
    redirect("Admin/Item");
  }

  function delete_item($id_item){
    $data = $this->M_admin->delete_item($id_item);
    if ($data) {
      $this->session->set_flashdata('success', 'item telah dihapus!');
    }
    else {
      $this->session->set_flashdata('error', 'Gagal menghapus item!');
    }
    redirect("Admin/Item");
  }

  function detail_notif($id_notif,$id_item,$status_item){
    $id_supp = $this->session->userdata('id');
    $header['notif']= $this->M_admin->get_notif($id_supp)->result();
    $header['jum_notif']=count($header['notif']);

    $notif = [
      'status_notif'=> 'terbaca'
    ];

    $header['item']= $this->M_admin->ajax_get_item($id_item);
    $header['status_item']=$status_item;

    $this->M_admin->update_notif($notif,$id_notif);

    $this->load->view('admin/head',$header);
    $this->load->view('admin/detail_notif',$header);

  }

}
