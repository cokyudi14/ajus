<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('M_user','M_supplier','M_admin'));
    if(!$this->session->userdata('admin_username')){
          redirect('admin/login');
    }
  }

  function index(){
    $data['user'] = $this->M_user->get_all_user();
    $data['role'] = $this->M_user->get_role();
    $data['supplier'] = $this->M_admin->get_jenis_item()->result();
    $this->load->view('admin/head');
    $this->load->view('admin/user',$data);
  }

  function ajax_get_user(){
    $data = $this->M_user->ajax_get_user();
    echo json_encode($data[0]);
  }

  function add_user($redirect){
    if ($this->input->post('role')=='supplier') {
      $jenis_item = $this->input->post('id_type_supplier');
    }else{
      $jenis_item = 0;
    }
    $data = $this->M_user->add_user($jenis_item);
    if ($data=='berhasil') {
      $this->session->set_flashdata('success', 'User baru telah ditambahkan!');
    }
    else if($data=='username_sama'){
      $this->session->set_flashdata('error', 'Username yang dimasukkan telah terdaftar sebelumnya!');
    }
    else if($data=='password_beda'){
      $this->session->set_flashdata('error', 'Password yang dimasukkan tidak sama!');
    }
    redirect("Admin/$redirect");
  }

  function update_user($redirect){
    $data = $this->M_user->edit_user();
    if ($this->input->post('role')=='supplier') {
      $supp = $this->M_supplier->edit_supplier();
    }
    if ($data=='berhasil') {
      $this->session->set_flashdata('success', 'User telah di Update!');
    }
    else {
      $this->session->set_flashdata('error', 'Gagal mengedit user!');
    }
    redirect("Admin/$redirect");
  }

  function delete_user($redirect,$username){
    $data = $this->M_user->delete_user($username);
    // if ($role=='supplier') {
    //   $supp = $this->M_supplier->delete_supplier($username);
    // }
    if ($data) {
      $this->session->set_flashdata('success', 'User telah dihapus!');
    }
    else {
      $this->session->set_flashdata('error', 'Gagal menghapus user!');
    }
    redirect("Admin/$redirect");
  }

  function profil(){
    $id_supp = $this->session->userdata('id');
    $data['user'] = $this->M_user->get_all_user();
    $data['role'] = $this->M_user->get_role();
    $data['supplier'] = $this->M_admin->get_jenis_item()->result();
    $header['notif']= $this->M_admin->get_notif($id_supp)->result();
    $header['jum_notif']=count($header['notif']);

    $data['profil']= $this->M_admin->profil($id_supp);


    $this->load->view('admin/head',$header);
    $this->load->view('admin/profil',$data);

  }

  function update_profil(){
    $file_name= $this->session->userdata('admin_username').'_'.time().'_'.$_FILES['foto']['name'];
    // Setup folder upload path
    $config['upload_path']    = './assets/images/profil/';

    // Setup file yang di izinkan
    $config['allowed_types']  = 'gif|jpg|png|jpeg';
    $config['file_name']= $file_name;
    
    // Memanggil library upload disertai dengan paramter $config array
    $this->load->library('upload', $config);
    $this->upload->do_upload('foto');


    $id_user = $this->input->post('id_supp');
    $update = [
      'nama'=>$this->input->post('nama'),
      'no_hp'=>$this->input->post('no_hp'),
      'alamat'=>$this->input->post('alamat'),
      'email'=>$this->input->post('email'),
      'kota'=>$this->input->post('kota'),
      'foto'=>$file_name
    ];


    $this->db->where('id_user', $id_user)->update('tb_user',$update);
    $this->session->set_userdata('admin_name',$this->input->post('nama'));
    $this->session->set_userdata('foto',$file_name);
    redirect("Admin/User/profil");
  }

}
