<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
     $this->load->model(array('M_user','M_supplier','M_admin'));
    if(!$this->session->userdata('admin_username')){
          redirect('admin/login');
    }
  }

  function index()
  {
    $this->db->where('id_role', 'member');
    $data['member'] = $this->db->count_all_results('tb_user');

    $this->db->where('id_role', 'supplier');
    $data['supplier'] = $this->db->count_all_results('tb_user');

    $id_supp = $this->session->userdata('id');
    $header['notif']= $this->M_admin->get_notif($id_supp)->result();
    $header['jum_notif']=count($header['notif']);
    
     $this->load->view('admin/head',$header);
    $this->load->view('admin/dashboard',$data);
  }

}
