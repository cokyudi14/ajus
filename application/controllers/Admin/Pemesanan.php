<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pemesanan extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('M_user','M_supplier','M_admin'));
    $this->load->library(array('upload'));
    $this->load->helper(array('url'));
    if(!$this->session->userdata('admin_username')){
          redirect('admin/login');
    }
  }

  function index(){
    $id_supp = $this->session->userdata('id');
    $id_item = $this->M_admin->get_IdItem($id_supp)->result();
    $header['notif']= $this->M_admin->get_notif($id_supp)->result();
    $header['jum_notif']=count($header['notif']);

    // $count=count($id_item);
    // if ($count==0) {
    //   $item='';
    // }
    // else if ($count==1) {
    //    foreach ($id_item as $id) {
    //     $item = 'id_item='.$id->id_item;
    //   }
    // }
    // else{
    //   $item='';
    //     foreach ($id_item as $id) {
    //     $item = 'id_item='.$id->id_item.' OR '.$item;
    //   }
    //   $item = substr($item, 0, -4);
    // }
   
    // $data['item'] = $this->M_admin->get_pemesanan_item($item)->result();


   //print_r($id_item); 
   //echo $item;
    $i=0;
    $data[0]='';
    foreach ($id_item as $id) {
      $data[$i] = $this->M_admin->get_pemesanan_item($id->id_item)->result();
      $data_p[$i] = $this->M_admin->get_pemesanan_paket_item($id->id_item)->result();
      $i++;      
       }
   // print_r($data[0]); 
       //$x=0;
       $datax['item']=$data[0];
       $datax['item_p']=$data[0];
       for ($x=0; $x < $i; $x++) { 
            $datax['item']=array_merge($datax['item'],$data[$x]);
            $datax['item_p']=array_merge($datax['item_p'],$data_p[$x]);
         }
      if ($i==0) {
          $datax['item']=array();
          $datax['item_p']=array();
        }  
               
       
       

       //print_r($data);

     $this->load->view('admin/head',$header);
     $this->load->view('admin/pemesanan',$datax);
  }

  function ajax_get_detail_transaksi(){
    $id_transaksi_detail = $this->input->post('id_transaksi_detail');
    $data = $this->M_admin->get_pemesanan_detail($id_transaksi_detail)->result();
    echo json_encode($data[0]);
  }

   function ajax_get_detail_transaksi_paket(){
    $id_transaksi_detail = $this->input->post('id_transaksi_detail');
    $data = $this->M_admin->get_pemesanan_detail_paket($id_transaksi_detail)->result();
    echo json_encode($data[0]);
  }

 
}
