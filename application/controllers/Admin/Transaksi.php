<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('M_user','M_supplier','M_admin'));
    $this->load->library(array('upload'));
    $this->load->helper(array('url'));
    if(!$this->session->userdata('admin_username')){
          redirect('admin/login');
    }
  }

  function index(){
    $role = $this->session->userdata('admin_role');
    $id_supp = $this->session->userdata('id');
    $data['transaksi'] = $this->M_admin->getAllTransaksi()->result();
    $data['transaksiPaket'] = $this->M_admin->getAllTransaksiPaket()->result();
    $this->load->view('admin/head');
    $this->load->view('admin/transaksi',$data);
  }

  public function detail($id)
  {
    $data['id'] = $id;
    $schedule = $this->db
      ->join('tb_item i', 'i.id_item = tpt.id_item', 'inner')
      ->where('tpt.id_transaksi_paket', $id)
      ->order_by('tgl_pemesanan', 'asc')
      ->get('tb_transaksi_paket_timeline tpt')
      ->result();

    $data['schedule_pesan'] = array();
    $data['schedule_ambil'] = array();
    foreach ($schedule as $key => $s) {
      if ($s->jenis == 'Pesan') {
        array_push($data['schedule_pesan'], $s);
      } else {
        array_push($data['schedule_ambil'], $s);
      }
    }
    // print_r($data['schedule_ambil']);

    $this->load->view('admin/head');
    $this->load->view('admin/timeline', $data);
  }

  public function schedule($id)
  {
    $isExist = $this->db
      ->where('id_transaksi_paket', $id)
      ->get('tb_transaksi_paket_timeline')
      ->num_rows();

    if ($isExist == 0) {
      $transaksi = $this->db
        ->select('tp.*, id_item, tgl_upacara')
        ->join('tb_transaksi_paket_detail tpd', 'tpd.id_transaksi_paket = tp.id_transaksi_paket', 'inner')
        ->where('tp.id_transaksi_paket', $id)
        ->get('tb_transaksi_paket tp')
        ->row();

      $item = $this->db
        ->join('tb_item i', 'i.id_item = pi.item_id', 'inner')
        ->where('paket_id', $transaksi->id_item)
        ->get('tb_paket_item pi')
        ->result();

      $schedule = [];
      $index = 0;
      foreach ($item as $key => $i) {
        $schedule[$index] = (object) [];
        $schedule[$index]->id_transaksi_paket = $transaksi->id_transaksi_paket;
        $schedule[$index]->id_item = $i->id_item;
        $schedule[$index]->jenis = 'Pesan';

        if ($i->lama_buat == 0) {
          $schedule[$index]->tgl_pemesanan = date('Y-m-d', strtotime('-1 day', strtotime($transaksi->tgl_upacara)));
          $index++;
        } else {
          $schedule[$index]->tgl_pemesanan = date('Y-m-d', strtotime('-'.$i->lama_buat.' day', strtotime($transaksi->tgl_upacara)));
          $index++;
        }
        $schedule[$index] = (object) [];
        $schedule[$index]->id_transaksi_paket = $transaksi->id_transaksi_paket;
        $schedule[$index]->id_item = $i->id_item;
        $schedule[$index]->jenis = 'Ambil Pesanan';
        $schedule[$index]->tgl_pemesanan = date('Y-m-d', strtotime('-1 day', strtotime($transaksi->tgl_upacara)));
        $index++;
      }
      $this->db->insert_batch('tb_transaksi_paket_timeline', $schedule);
    }
    redirect('Admin/Transaksi/detail/'.$id);
    echo json_encode([$transaksi, $schedule, $item]);
  }

  function ajax_get_detail(){
    $id_transaksi_paket = $this->input->post('id_transaksi_paket');
    $data = $this->M_admin->ajax_get_transaksi_paket($id_transaksi_paket);
    echo json_encode($data[0]);
  }

  function ajax_get_detailItem(){
    $id_transaksi = $this->input->post('id_transaksi');
    $data = $this->M_admin->ajax_get_transaksi($id_transaksi);
    echo json_encode($data[0]);
  }

  function ajax_get_detail_paket(){
    $id_paket = $this->input->post('id_paket');
    $data2 = $this->M_admin->get_detail_item_paket($id_paket);
    echo json_encode($data2);
  }
   function ajax_get_detail_item(){
    $id_paket = $this->input->post('id_paket');
    $data2 = $this->M_admin->get_detail_item_paket($id_paket);
    echo json_encode($data2);
  }


}
