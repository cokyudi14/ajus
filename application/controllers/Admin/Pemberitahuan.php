<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pemberitahuan extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('M_supplier','M_admin'));
    if(!$this->session->userdata('admin_username')){
          redirect('admin/login');
    }
  }

  function index(){
    $role = $this->session->userdata('admin_role');
    $id_jenis = $this->session->userdata('jenis_item'); 
    $id_supp = $this->session->userdata('id');
    $header['notif']= $this->M_admin->get_notif($id_supp)->result();
    $header['jum_notif']=count($header['notif']);

    if ($role == 'admin') {
      $schedule = $this->db
        ->join('tb_item i', 'i.id_item = tpt.id_item', 'inner')
        ->join('tb_user u', 'u.id_user = i.id_supplier', 'inner')
        ->join('tb_transaksi_paket tp', 'tp.id_transaksi_paket = tpt.id_transaksi_paket', 'inner')
        ->join('tb_member m', 'm.id_member = tp.id_member', 'inner')
        ->where('tpt.tgl_pemesanan >= DATE(NOW())')
        ->order_by('tgl_pemesanan', 'asc')
        ->get('tb_transaksi_paket_timeline tpt')
        ->result();
    } else {
      $schedule = $this->db
        ->join('tb_item i', 'i.id_item = tpt.id_item', 'inner')
        ->join('tb_user u', 'u.id_user = i.id_supplier', 'inner')
        ->join('tb_transaksi_paket tp', 'tp.id_transaksi_paket = tpt.id_transaksi_paket', 'inner')
        ->join('tb_member m', 'm.id_member = tp.id_member', 'inner')
        ->where('i.id_supplier', $id_supp)
        ->where('tpt.tgl_pemesanan >= DATE(NOW())')
        ->order_by('tgl_pemesanan', 'asc')
        ->get('tb_transaksi_paket_timeline tpt')
        ->result();
    }

    $data['schedule_pesan'] = array();
    $data['schedule_ambil'] = array();
    foreach ($schedule as $key => $s) {
      if ($s->jenis == 'Pesan') {
        array_push($data['schedule_pesan'], $s);
      } else {
        array_push($data['schedule_ambil'], $s);
      }
    }
    
    $this->load->view('admin/head',$header);
    $this->load->view('admin/pemberitahuan',$data);
  }

}
