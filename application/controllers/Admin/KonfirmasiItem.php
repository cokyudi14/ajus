<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KonfirmasiItem extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('M_user','M_supplier','M_admin'));
    $this->load->library(array('upload'));
    $this->load->helper(array('url'));
    if(!$this->session->userdata('admin_username')){
          redirect('admin/login');
    }
  }

  function index(){
    $role = $this->session->userdata('admin_role');
    $data['item'] = $this->M_admin->get_all_KonfirmasiItem()->result();
    $this->load->view('admin/head');
    $this->load->view('admin/konfirmasiItem',$data);
  }

  function tolak_item($id_item){
    
    $update = [
      'item_status'=>'ditolak'
    ];

    $date = date("Y-m-d");
    $id_sup = $this->M_admin->get_IdSupp($id_item)->result();

    foreach ($id_sup as $key) {
      $supplier_id = $key->id_supplier;      
    }
    $notif = [
      'item_id'=> $id_item,
      'tanggal'=> $date,
      'status_item'=> 'ditolak',
      'status_notif'=> 'terkirim',
      'supplier_id'=> $supplier_id
    ];


    $data = $this->M_admin->edit_item($update,$id_item);
    $this->M_admin->notif($notif);
    if ($data=='berhasil') {
      $this->session->set_flashdata('success', 'Item telah di Update!');
    }
    else {
      $this->session->set_flashdata('error', 'Gagal Update item!');
    }
    redirect("Admin/KonfirmasiItem");
  }

  function terima_item($id_item){
    
    $update = [
      'item_status'=>'diterima'
    ];

    $date = date("Y-m-d");
    $id_sup = $this->M_admin->get_IdSupp($id_item)->result();
     foreach ($id_sup as $key) {
      $supplier_id = $key->id_supplier;    
    }
    $notif = [
      'item_id'=> $id_item,
      'tanggal'=> $date,
      'status_item'=> 'diterima',
      'status_notif'=> 'terkirim',
      'supplier_id'=> $supplier_id
    ];

    $data = $this->M_admin->edit_item($update,$id_item);
    $this->M_admin->notif($notif);
    if ($data=='berhasil') {
      $this->session->set_flashdata('success', 'Item telah di Update!');
    }
    else {
      $this->session->set_flashdata('error', 'Gagal Update item!');
    }
    redirect("Admin/KonfirmasiItem");
  }


}