<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('M_admin'));
    //Codeigniter : Write Less Do More
    if(!$this->session->userdata('admin_username')){
          redirect('admin/login');
    }
  }

  function index()
  {
    $data['supplier'] = $this->M_admin->get_supplier();
    $data['type'] = $this->M_admin->get_jenis_item()->result();
    $this->load->view('admin/head');
    $this->load->view('admin/supplier',$data);
  }

  function ajax_get_supp(){
    $data = $this->M_admin->ajax_get_supp();
    echo json_encode($data[0]);
  }

}
