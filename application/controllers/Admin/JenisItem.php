<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class JenisItem extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('M_user','M_supplier','M_admin'));
    if(!$this->session->userdata('admin_username')){
          redirect('admin/login');
    }
  }

  function index(){
    $data['jenisItem'] = $this->M_admin->get_all_jenis_item()->result();
    $this->load->view('admin/head');
    $this->load->view('admin/jenis_item',$data);
  }

  function ajax_get_jenis_item(){
    $id_jenis_item = $this->input->post('id_jenis_item');
    $data = $this->M_admin->ajax_get_jenis_item($id_jenis_item);
    echo json_encode($data[0]);
  }

  function add_jenis_item(){
    $data = $this->M_admin->add_jenis_item();

    if ($data=='berhasil') {
      $this->session->set_flashdata('success', 'Jenis Item baru telah ditambahkan!');
    }
    else if($data=='jenis_item_sama'){
      $this->session->set_flashdata('error', 'Jenis Item yang dimasukkan telah terdaftar sebelumnya!');
    }
    redirect("Admin/JenisItem");
  }

  function update_jenis_item(){
    $data = $this->M_admin->edit_jenis_item();
    if ($data=='berhasil') {
      $this->session->set_flashdata('success', 'Jenis Item telah di Update!');
    }
    else {
      $this->session->set_flashdata('error', 'Gagal mengedit jenis item!');
    }
    redirect("Admin/JenisItem");
  }

  function delete_jenis_item($id_jenis_item){
    $data = $this->M_admin->delete_jenis_item($id_jenis_item);
    if ($data) {
      $this->session->set_flashdata('success', 'Jenis item telah dihapus!');
    }
    else {
      $this->session->set_flashdata('error', 'Gagal menghapus jenis item!');
    }
    redirect("Admin/JenisItem");
  }

}
