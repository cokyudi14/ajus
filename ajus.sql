-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 19, 2018 at 03:51 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ajus`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_item`
--

CREATE TABLE `tb_item` (
  `id_item` int(11) NOT NULL,
  `nama_item` varchar(255) NOT NULL,
  `jenis_item` int(3) NOT NULL,
  `id_supplier` int(3) NOT NULL,
  `lama_buat` int(32) NOT NULL,
  `lama_tahan` int(32) NOT NULL,
  `harga_modal` float DEFAULT NULL,
  `harga_jual` float DEFAULT NULL,
  `foto_item` varchar(255) DEFAULT NULL,
  `deskripsi` text,
  `id_upacara` int(5) DEFAULT NULL,
  `status` enum('tidak promo','promo') NOT NULL,
  `jenis_transaksi` varchar(20) NOT NULL,
  `item_status` enum('diterima','ditolak','tertunda') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_item`
--

INSERT INTO `tb_item` (`id_item`, `nama_item`, `jenis_item`, `id_supplier`, `lama_buat`, `lama_tahan`, `harga_modal`, `harga_jual`, `foto_item`, `deskripsi`, `id_upacara`, `status`, `jenis_transaksi`, `item_status`) VALUES
(1, 'Gitar', 2, 4, 0, 0, 200000, 250000, 'Gitar.jpg', 'Ini adalah gitar bolong', NULL, 'tidak promo', '', 'diterima'),
(3, 'bass', 2, 5, 4, 2, 200000, 200000, 'bass.png', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(4, 'Kursi Plastik (kanpa kain)', 1, 2, 0, 0, 3000, 3000, 'Kursi-Plastik-kanpa-kain.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(5, 'Kursi Plastik (plus kain)', 1, 2, 0, 0, 5000, 5000, 'Kursi-Plastik-plus-kain.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(6, 'Kursi Besi (tanpa kain)', 1, 2, 0, 0, 10, 10, 'Kursi-Besi-tanpa-kain.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(7, 'Kursi Besi (plus kain)', 1, 2, 0, 0, 15, 15, 'Kursi-Besi-plus-kain.jpg', NULL, NULL, 'tidak promo', 'jual', 'diterima'),
(8, 'meja bundar kayu (lengkap aksesoris)', 1, 2, 0, 0, 70, 70, 'meja-bundar-kayu-lengkap-aksesoris.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(9, 'Meja Panjang Kayu (lengkap aksesoris)', 1, 2, 0, 0, 40, 40, 'Meja-Panjang-Kayu-lengkap-aksesoris.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(10, 'Mini Panggung (plus karpet)', 1, 2, 0, 0, 70, 70, 'Mini-Panggung-plus-karpet.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(11, 'Gayor/Ukiran (lengkap)', 1, 2, 0, 0, 5, 5, 'GayorUkiran-lengkap.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(12, 'Sarung Kursi', 1, 2, 0, 0, 3, 3, 'Sarung-Kursi.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(13, 'Sarung Meja (bundar/panjang)', 1, 2, 0, 0, 5, 5, 'Sarung-Meja-bundarpanjang.jfif', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(14, 'Kursi Plastik (tanpa kain)', 7, 2, 0, 0, 3, 3000000, 'Kursi-Plastik-tanpa-kain.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(15, 'Kursi Plastik (plus kain)', 7, 2, 0, 0, 5, 5, 'Kursi-Plastik-plus-kain.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(16, 'Kursi Besi (tanpa kain)', 7, 2, 0, 0, 10, 10, 'Kursi-Besi-tanpa-kain.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(17, 'Kursi Besi (plus kain)', 7, 2, 0, 0, 15, 15, 'Kursi-Besi-plus-kain.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(18, 'Meja Bundar Kayu (lengkap aksesoris)', 7, 2, 0, 0, 70, 70, 'Meja-Bundar-Kayu-lengkap-aksesoris.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(19, 'Meja Panjang Kayu/Meja Welcome Drink (lengkap aksesoris)', 7, 2, 0, 0, 40, 40, 'Meja-Panjang-KayuMeja-Welcome-Drink-lengkap-aksesoris.jpg', NULL, NULL, 'tidak promo', 'jual', 'diterima'),
(20, 'Meja Besi Persegi (lengkap aksesoris)', 7, 2, 0, 0, 40, 40, 'Meja-Besi-Persegi-lengkap-aksesoris.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(21, 'Tenda 4x4 (tanpa kain)', 7, 2, 0, 0, 150, 150, 'Tenda-4x4-tanpa-kain.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(22, 'Tenda  4x4 (plus kain)', 7, 2, 0, 0, 300, 300, 'Tenda-4x4-plus-kain.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(23, 'Tendai 6x4 (tanpa kain)', 7, 2, 0, 0, 150, 150, 'Tendai-6x4-tanpa-kain.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(24, 'Tenda  6x4 (plus kain)', 7, 2, 0, 0, 300, 300, 'Tenda-6x4-plus-kain.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(27, 'Gayor/Ukiran (lengkap)', 7, 2, 0, 0, 5, 5, 'GayorUkiran-lengkap.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(28, 'Mini Panggung 8x8 (plus karpet)', 7, 2, 0, 0, 70, 70, 'Mini-Panggung-8x8-plus-karpet.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(29, 'Sarung Kursi', 7, 2, 0, 0, 3, 3, 'Sarung-Kursi.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(31, 'Speaker Aktif', 10, 4, 0, 0, 200, 200, 'Speaker-Aktif.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(32, 'Mixer', 10, 4, 0, 0, 200, 200, 'Mixer.jpg', NULL, NULL, 'tidak promo', 'jual', 'diterima'),
(33, 'Microphone', 10, 4, 0, 0, 75, 75, 'Microphone.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(34, 'Kabel Microphone', 10, 4, 0, 0, 50, 50, 'Kabel-Microphone.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(35, 'Stand Microphone', 10, 4, 0, 0, 100, 100, 'Stand-Microphone.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(36, 'Kabel Speaker', 10, 4, 0, 0, 75, 75, 'Kabel-Speaker.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(37, 'DI Box', 10, 4, 0, 0, 50, 50, 'DI-Box.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(38, 'Kabel Guitar', 10, 4, 0, 0, 50, 50, 'Kabel-Guitar.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(39, 'Stand Speaker', 10, 4, 0, 0, 150, 150, 'Stand-Speaker.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(40, 'Stand Mic Cajon', 10, 4, 0, 0, 50, 50, 'Stand-Mic-Cajon.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(41, 'Cajon', 10, 4, 0, 0, 75, 75, 'Cajon.jpeg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(42, 'Guitar Acoustic', 10, 4, 0, 0, 100, 100, 'Guitar-Acoustic.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(43, 'Keyboard', 10, 4, 0, 0, 200, 200, 'Keyboard.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(44, 'Stand Keyboard', 10, 4, 0, 0, 75, 75, 'Stand-Keyboard.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(45, 'Rias Prewedding  (couple/pawiwahan)', 12, 5, 0, 0, 2.5, 2.5, 'Rias-Prewedding-couplepawiwahan.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(46, 'Rias Ngidih (couple/pawiwahan)', 12, 5, 0, 0, 2.5, 2.5, 'Rias-Ngidih-couplepawiwahan.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(47, 'Rias Madya (couple/pawiwahan)', 12, 5, 0, 0, 2.5, 2.5, 'Rias-Madya-couplepawiwahan.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(48, 'Rias Agung (couple/pawiwahan)', 12, 5, 0, 0, 6.5, 6.5, 'Rias-Agung-couplepawiwahan.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(49, 'Rias Modifikasi (couple/pawiwahan)', 12, 5, 0, 0, 5, 5, 'Rias-Modifikasi-couplepawiwahan.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(50, 'Sekaa Rindik ( 2org rindik + 1org suling)', 11, 7, 0, 0, 1, 1, 'Sekaa-Rindik-2org-rindik-1org-suling.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(51, 'Sekaa Rindik ( 2org rindik + 2org suling)', 11, 7, 0, 0, 1.2, 1.2, 'Sekaa-Rindik-2org-rindik-2org-suling.jpg', NULL, NULL, 'tidak promo', 'jual', 'diterima'),
(52, 'Sekaa Gender ( 2org Gender)', 11, 7, 0, 0, 1, 1, 'Sekaa-Gender-2org-Gender.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(53, 'Sekaa Gender ( 4org Gender)', 11, 7, 0, 0, 1.5, 1.5, 'Sekaa-Gender-4org-Gender.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(54, 'seka rindik + joged (2org rindik+1org suling+2org joged)', 11, 7, 0, 0, 3.5, 3.5, 'seka-rindik-joged-2org-rindik1org-suling2org-joged.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(55, 'joged', 11, 7, 0, 0, 500, 500, 'joged.jpg', NULL, NULL, 'tidak promo', 'sewa', 'diterima'),
(56, 'Nasi Putih (1 baskom)', 8, 3, 1, 2, 35, 35, 'Nasi-Putih-1-baskom.jpg', NULL, NULL, 'tidak promo', 'jual', 'diterima'),
(57, 'Nasi Kuning (1 baskom)', 8, 3, 1, 2, 40, 40, 'Nasi-Kuning-1-baskom.jpg', NULL, NULL, 'tidak promo', 'jual', 'diterima'),
(58, 'Urab Jamur', 8, 3, 1, 1, 20, 20, 'Urab-Jamur.jpg', NULL, NULL, 'tidak promo', 'jual', 'diterima'),
(59, 'Capcay', 8, 3, 1, 1, 20, 20, 'Capcay.jpg', NULL, NULL, 'tidak promo', 'jual', 'diterima'),
(60, 'Bakso', 8, 3, 1, 2, 20, 20, 'Bakso.jpg', NULL, NULL, 'tidak promo', 'jual', 'diterima'),
(61, 'Rendang Babi', 8, 3, 1, 2, 30, 30, 'Rendang-Babi.jpg', NULL, NULL, 'tidak promo', 'jual', 'diterima'),
(62, 'Rendang Ayam', 8, 3, 1, 2, 30, 30, 'Rendang-Ayam.jpg', NULL, NULL, 'tidak promo', 'jual', 'diterima'),
(63, 'Udang Tepung', 8, 3, 1, 2, 25000, 25000, 'Udang-Tepung.jpg', NULL, NULL, 'tidak promo', 'jual', 'diterima'),
(64, 'Sate Tusuk Babi (10 tusuk/porsi)', 8, 3, 1, 2, 25000, 25000, 'Sate-Tusuk-Babi-10-tusukporsi.jpg', NULL, NULL, 'tidak promo', 'jual', 'diterima'),
(65, 'Sate Tusuk Ayam (10 tusuk/porsi)', 8, 3, 1, 2, 25000, 25000, 'Sate-Tusuk-Ayam-10-tusukporsi.jpg', NULL, NULL, 'tidak promo', 'jual', 'diterima'),
(66, 'Sate Lilit (10 sate/porsi)', 8, 3, 1, 2, 30, 30, 'Sate-Lilit-10-sateporsi.jpg', NULL, NULL, 'tidak promo', 'jual', 'diterima'),
(67, 'Dessert EsCream (1 escream)', 8, 3, 0, 0, 3.5, 3.5, 'Dessert-EsCream-1-escream.jpg', NULL, NULL, 'tidak promo', 'jual', 'diterima'),
(68, 'Dessert Buah (1buah jeruk+1buah semangka)', 8, 3, 0, 0, 4, 4, 'Dessert-Buah-1buah-jeruk1buah-semangka.jpg', NULL, NULL, 'tidak promo', 'jual', 'diterima'),
(69, 'Banten Kumara (bayi tiga bulanan)', 9, 6, 2, 2, 95, 95, 'Banten-Kumara-bayi-tiga-bulanan.jpg', NULL, NULL, 'tidak promo', 'jual', 'diterima'),
(70, 'Banten Byakala (upacara bayi tigabulanan)', 9, 6, 2, 2, 50, 50, 'Banten-Byakala-upacara-bayi-tigabulanan.jpg', NULL, NULL, 'tidak promo', 'jual', 'diterima'),
(71, 'Daksina ', 9, 6, 0, 2, 15, 15, 'Daksina.jpg', NULL, NULL, 'tidak promo', 'jual', 'diterima'),
(72, 'Canang (25 biji)', 9, 6, 0, 1, 10, 10, 'Canang-25-biji.jpg', NULL, NULL, 'tidak promo', 'jual', 'diterima'),
(73, 'Banten Penyambutan', 9, 6, 3, 2, 150, 150, 'Banten-Penyambutan.jpg', NULL, NULL, 'tidak promo', 'jual', 'diterima'),
(74, 'Banten Sayut Pangulapan', 9, 6, 2, 2, 50, 50, 'Banten-Sayut-Pangulapan.jpg', NULL, NULL, 'tidak promo', 'jual', 'diterima'),
(75, 'Banten Bayi Tiga Bulanan (lengkap)', 9, 6, 7, 2, 5, 5, 'Banten-Bayi-Tiga-Bulanan-lengkap.jpg', NULL, NULL, 'tidak promo', 'jual', 'diterima'),
(76, 'Paket Mantap/porsi (nasi putih,sate lilit,capcay,cah tahu,bakso,udang tepung, eskrim) ', 8, 3, 7, 1, 55, 55, 'Paket-Mantapporsi-nasi-putihsate-lilitcapcaycah-tahubaksoudang-tepung-eskrim.jpg', NULL, NULL, 'tidak promo', 'jual', 'diterima'),
(77, 'Soundsystem Lengkap', 10, 4, 2, 0, 2.5, 2.5, 'Soundsystem-Lengkap.jpg', NULL, NULL, 'tidak promo', 'sewa', 'tertunda'),
(78, 'Banten Pawiwahan (lengkap+pemuput upacara)', 9, 6, 7, 1, 7000000, 7000000, 'Banten-Pawiwahan-lengkappemuput-upacara.jpg', 'banten lengkap dengan standar upacara pawiwahan', NULL, 'tidak promo', 'jual', 'diterima');

-- --------------------------------------------------------

--
-- Table structure for table `tb_jenis_item`
--

CREATE TABLE `tb_jenis_item` (
  `id_jenis_item` int(32) NOT NULL,
  `jenis_item` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_jenis_item`
--

INSERT INTO `tb_jenis_item` (`id_jenis_item`, `jenis_item`) VALUES
(7, 'Dekorasi'),
(8, 'Catering'),
(9, 'Bebantenan'),
(10, 'Soundsystem & alat musik'),
(11, 'sekaa tabuh'),
(12, 'Salon');

-- --------------------------------------------------------

--
-- Table structure for table `tb_konfirmasi`
--

CREATE TABLE `tb_konfirmasi` (
  `id_konfirmasi` int(255) NOT NULL,
  `id_transaksi` int(255) DEFAULT NULL,
  `bank_tujuan` varchar(255) DEFAULT NULL,
  `bank_asal` varchar(255) DEFAULT NULL,
  `pemilik_rek` varchar(255) DEFAULT NULL,
  `no_rek` varchar(255) DEFAULT NULL,
  `bukti_transfer` varchar(255) DEFAULT NULL,
  `tgl_konfirmasi` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_member`
--

CREATE TABLE `tb_member` (
  `id_member` int(255) NOT NULL,
  `nama_member` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `telp` varchar(20) DEFAULT NULL,
  `kota` varchar(50) DEFAULT NULL,
  `kode_pos` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_member`
--

INSERT INTO `tb_member` (`id_member`, `nama_member`, `email`, `password`, `alamat`, `telp`, `kota`, `kode_pos`) VALUES
(2, 'ajus', 'amrita@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 'batubulan', '08551115511', 'Gianyar, Bali', 12345678),
(3, 'Gung De', 'gungde@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 'sss', '08123', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_notif`
--

CREATE TABLE `tb_notif` (
  `id_notif` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `status_item` enum('ditolak','diterima') DEFAULT NULL,
  `status_notif` enum('terkirim','terbaca') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_notif`
--

INSERT INTO `tb_notif` (`id_notif`, `item_id`, `supplier_id`, `tanggal`, `status_item`, `status_notif`) VALUES
(5, 76, 4, '2018-10-28', 'ditolak', 'terbaca'),
(6, 77, 4, '2018-10-28', 'ditolak', 'terbaca'),
(7, 76, 3, '2018-10-28', 'ditolak', 'terbaca'),
(8, 76, 3, '2018-10-28', 'diterima', 'terbaca'),
(9, 78, 6, '2018-12-14', 'diterima', 'terkirim');

-- --------------------------------------------------------

--
-- Table structure for table `tb_paket`
--

CREATE TABLE `tb_paket` (
  `id_paket` int(255) NOT NULL,
  `upacara_id` int(11) NOT NULL,
  `nama_paket` varchar(100) DEFAULT NULL,
  `harga_paket` float DEFAULT NULL,
  `lama_buat_paket` int(5) DEFAULT NULL,
  `foto_paket` varchar(100) DEFAULT NULL,
  `status` enum('tidak promo','promo') NOT NULL,
  `deskripsi` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_paket`
--

INSERT INTO `tb_paket` (`id_paket`, `upacara_id`, `nama_paket`, `harga_paket`, `lama_buat_paket`, `foto_paket`, `status`, `deskripsi`) VALUES
(1, 1, 'Paket Manusa Yadnya', 100000, 11, 'Paket-Manusa-Yadnya.png', 'promo', NULL),
(3, 9, 'Paket Pawiwahan I', 30000000, NULL, 'Paket-Pawiwahan-I.jpg', 'tidak promo', 'Paket pawiahan ini sudah lengkap dengan standar harga yang kami beri'),
(4, 9, 'paket pawiwahan II', 40000000, NULL, 'paket-pawiwahan-II.jpg', 'tidak promo', 'isi paket lengkap dan sesuai dengan standar harga yang kami beri'),
(5, 9, 'Paket Ngidih I', 15000000, NULL, 'Paket-Ngidih-I.jpg', 'tidak promo', 'paket ngidih lengkap sesuai standar upacara adat agama hindu'),
(6, 9, 'Paket ngidih II', 20000000, NULL, 'Paket-ngidih-II.jpg', 'tidak promo', 'paket ngidih lengkap dengan standar upacara adat agama hindu di Bali'),
(7, 8, 'paket metatah I', 20000000, NULL, 'paket-metatah-I.jpg', 'tidak promo', 'paket metatah dengan sesuai standar upacara adat agama hindu fi Bali'),
(8, 8, 'paket metatah II', 30000000, NULL, 'paket-metatah-II.JPG', 'tidak promo', 'paket metatah sesuai dengan standar upacara adat agama hindu di Bali'),
(9, 6, 'Paket Melaspas Rumah I', 8000000, NULL, 'Paket-Melaspas-Rumah-I.jpg', 'tidak promo', 'paket melaspas rumah sesuai dengan standar upacara adat agama hindu di Bali');

-- --------------------------------------------------------

--
-- Table structure for table `tb_paket_item`
--

CREATE TABLE `tb_paket_item` (
  `id_paket_item` int(255) NOT NULL,
  `paket_id` int(255) DEFAULT NULL,
  `item_id` int(255) DEFAULT NULL,
  `qty` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_paket_item`
--

INSERT INTO `tb_paket_item` (`id_paket_item`, `paket_id`, `item_id`, `qty`) VALUES
(4, 2, 1, 3),
(5, 2, 21, 2),
(6, 1, 15, 50),
(7, 1, 27, 1),
(10, 1, 75, 1),
(11, 1, 77, 1),
(12, 1, 76, 1),
(14, 3, 22, 2),
(15, 3, 15, 50),
(16, 3, 18, 2),
(17, 3, 19, 1),
(18, 3, 28, 1),
(19, 3, 76, 100),
(20, 3, 50, 1),
(21, 3, 48, 1),
(22, 3, 78, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_pembayaran`
--

CREATE TABLE `tb_pembayaran` (
  `id_pembayaran` int(255) NOT NULL,
  `id_member` int(255) DEFAULT NULL,
  `id_transaksi` int(255) DEFAULT NULL,
  `id_transaksi_paket` int(255) DEFAULT NULL,
  `bank_tujuan` varchar(50) DEFAULT NULL,
  `pemilik_rekening` varchar(100) DEFAULT NULL,
  `nomor_rekening` varchar(50) DEFAULT NULL,
  `bukti_pembayaran` varchar(50) DEFAULT NULL,
  `tgl_pembayaran` date DEFAULT NULL,
  `status_pembayaran` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pembayaran`
--

INSERT INTO `tb_pembayaran` (`id_pembayaran`, `id_member`, `id_transaksi`, `id_transaksi_paket`, `bank_tujuan`, `pemilik_rekening`, `nomor_rekening`, `bukti_pembayaran`, `tgl_pembayaran`, `status_pembayaran`) VALUES
(10, 3, 0, 14, 'mandiri', 'gungde', '2141224', '1540122499.PNG', '2018-10-21', 2),
(11, 1, 0, 15, 'mandiri', 'cok', '1241212', '1540123106.PNG', '2018-10-21', 2),
(12, 1, 0, 16, 'mandiri', 'aa', '2222', '1543235820.png', '2018-11-26', 2),
(13, 1, 1, 0, 'bca', 'kim', '3222', '1543237813.png', '2018-11-26', 2),
(14, 1, 2, 20, 'mandiri', 'aax', '222', '1543643165.JPG', '2018-12-01', 2),
(15, 1, 3, 21, 'mandiri', 'aa', '1222', '1543644640.JPG', '2018-12-01', 2),
(16, 2, 0, 22, 'mandiri', 'suastika', '123454321', '1544770057.jpg', '2018-12-14', 2),
(17, 2, 0, 23, 'bca', 'suastika', '098765678', '1544776805.jpg', '2018-12-14', 2),
(18, 2, 0, 24, 'bpd', 'suastika', '1234567', '1544883844.jpg', '2018-12-15', 1),
(19, 2, 4, 0, 'bca', 'fg', '12', '1544884237.jpg', '2018-12-15', 1),
(20, 2, 5, 0, 'bpd', NULL, NULL, NULL, '2018-12-18', 0),
(21, 2, 6, 0, 'mandiri', NULL, NULL, NULL, '2018-12-18', 0),
(22, 2, 0, 25, 'bca', NULL, NULL, NULL, '2018-12-18', 0),
(23, 2, 7, 0, 'mandiri', NULL, NULL, NULL, '2018-12-18', 0),
(24, 2, 0, 26, 'bca', NULL, NULL, NULL, '2018-12-19', 0),
(25, 2, 0, 27, 'bca', NULL, NULL, NULL, '2018-12-19', 0),
(26, 2, 0, 28, 'bpd', NULL, NULL, NULL, '2018-12-19', 0),
(27, 2, 0, 29, 'bpd', NULL, NULL, NULL, '2018-12-19', 0),
(28, 2, 8, 0, 'mandiri', 'suastika', '112234', '1545230074.', '2018-12-19', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_role`
--

CREATE TABLE `tb_role` (
  `id_role` varchar(16) NOT NULL,
  `role` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_role`
--

INSERT INTO `tb_role` (`id_role`, `role`) VALUES
('admin', 'Administrator'),
('member', 'Member'),
('supplier', 'Supplier');

-- --------------------------------------------------------

--
-- Table structure for table `tb_supplier`
--

CREATE TABLE `tb_supplier` (
  `id_type_supplier` varchar(32) NOT NULL,
  `type_supplier` varchar(32) NOT NULL,
  `username` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_supplier`
--

INSERT INTO `tb_supplier` (`id_type_supplier`, `type_supplier`, `username`) VALUES
('supp_alat_musik', 'Alat Musik', ''),
('supp_banten', 'Banten', 'gg'),
('supp_catering', 'Catering', ''),
('supp_dekorasi', 'Dekorasi', ''),
('supp_joged', 'Joged', ''),
('supp_meja', 'Meja', ''),
('supp_pendeta', 'Pendeta', ''),
('supp_pengisi_acara', 'Pengisi Acara', ''),
('supp_sound', 'Sound  System', ''),
('supp_tenda', 'Tenda', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_transaksi`
--

CREATE TABLE `tb_transaksi` (
  `id_transaksi` int(255) NOT NULL,
  `id_member` int(255) DEFAULT NULL,
  `tgl_transaksi` date DEFAULT NULL,
  `total_harga` float DEFAULT NULL,
  `status_transaksi` int(2) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_transaksi`
--

INSERT INTO `tb_transaksi` (`id_transaksi`, `id_member`, `tgl_transaksi`, `total_harga`, `status_transaksi`) VALUES
(3, 1, '2018-12-01', 0, 1),
(4, 2, '2018-12-15', 0, 1),
(5, 2, '2018-12-18', 303000000, 1),
(6, 2, '2018-12-18', 150000000, 1),
(7, 2, '2018-12-18', 30, 1),
(8, 2, '2018-12-19', 51.2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_transaksi_detail`
--

CREATE TABLE `tb_transaksi_detail` (
  `id_transaksi_detail` int(255) NOT NULL,
  `id_item` int(255) DEFAULT NULL,
  `qty_item` int(10) DEFAULT NULL,
  `id_transaksi` int(255) DEFAULT NULL,
  `harga` float DEFAULT NULL,
  `pesan_detail` text,
  `tgl_upacara` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_transaksi_detail`
--

INSERT INTO `tb_transaksi_detail` (`id_transaksi_detail`, `id_item`, `qty_item`, `id_transaksi`, `harga`, `pesan_detail`, `tgl_upacara`) VALUES
(3, 33, NULL, 3, 75, 'asik', '2018-07-01'),
(4, 14, NULL, 4, 3000000, 'ff', '2018-12-31'),
(5, 14, 1, 5, 3000000, NULL, '2018-12-08'),
(6, 14, 50, 5, 3000000, NULL, '2018-12-05'),
(7, 14, 50, 5, 3000000, NULL, '2018-12-05'),
(8, 14, 50, 6, 3000000, NULL, '2018-12-05'),
(9, 16, 3, 7, 10, NULL, '2018-11-26'),
(10, 37, 1, 8, 50, NULL, '2018-12-31'),
(11, 51, 1, 8, 1.2, NULL, '2018-12-31');

-- --------------------------------------------------------

--
-- Table structure for table `tb_transaksi_paket`
--

CREATE TABLE `tb_transaksi_paket` (
  `id_transaksi_paket` int(255) NOT NULL,
  `id_member` int(255) DEFAULT NULL,
  `tgl_transaksi` date DEFAULT NULL,
  `total_harga` float DEFAULT NULL,
  `status_transaksi` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_transaksi_paket`
--

INSERT INTO `tb_transaksi_paket` (`id_transaksi_paket`, `id_member`, `tgl_transaksi`, `total_harga`, `status_transaksi`) VALUES
(21, 1, '2018-12-01', 100000, 1),
(22, 2, '2018-12-14', 28000, 1),
(23, 2, '2018-12-14', 30000000, 1),
(24, 2, '2018-12-15', 40000000, 1),
(25, 2, '2018-12-18', 0, 1),
(26, 2, '2018-12-19', 0, 1),
(27, 2, '2018-12-19', 0, 1),
(28, 2, '2018-12-19', 0, 1),
(29, 2, '2018-12-19', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_transaksi_paket_detail`
--

CREATE TABLE `tb_transaksi_paket_detail` (
  `id_transaksi_paket_detail` int(255) NOT NULL,
  `id_item` int(255) DEFAULT NULL,
  `id_transaksi_paket` int(255) DEFAULT NULL,
  `harga` float DEFAULT NULL,
  `pesan_detail` text,
  `tgl_upacara` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_transaksi_paket_detail`
--

INSERT INTO `tb_transaksi_paket_detail` (`id_transaksi_paket_detail`, `id_item`, `id_transaksi_paket`, `harga`, `pesan_detail`, `tgl_upacara`) VALUES
(48, 1, 21, 100000, 'ada aja', '2018-12-29'),
(49, 2, 22, 28000, 'cc', '2018-07-10'),
(50, 3, 23, 30000000, 'uuy', '2018-07-11'),
(51, 4, 24, 40000000, 'qq', '2018-12-19'),
(52, 3, 25, 30000000, NULL, '2018-07-10'),
(53, 3, 25, 30000000, NULL, '2018-12-05'),
(54, 3, 26, 30000000, NULL, '2018-12-12'),
(55, 3, 27, 30000000, NULL, '2018-12-05'),
(56, 3, 28, 30000000, NULL, '2018-12-15'),
(57, 3, 29, 30000000, NULL, '2018-12-15');

-- --------------------------------------------------------

--
-- Table structure for table `tb_transaksi_paket_item`
--

CREATE TABLE `tb_transaksi_paket_item` (
  `id_transaksi_paket_item` int(11) NOT NULL,
  `id_transaksi_paket` int(11) NOT NULL,
  `id_item` int(11) NOT NULL,
  `tgl_upacara` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_transaksi_paket_item`
--

INSERT INTO `tb_transaksi_paket_item` (`id_transaksi_paket_item`, `id_transaksi_paket`, `id_item`, `tgl_upacara`) VALUES
(13, 21, 15, '2018-07-01'),
(14, 21, 27, '2018-07-01'),
(15, 21, 75, '2018-07-01'),
(16, 21, 77, '2018-07-01'),
(17, 21, 76, '2018-07-01'),
(18, 22, 1, '2018-07-10'),
(19, 22, 21, '2018-07-10'),
(20, 23, 22, '2018-07-11'),
(21, 23, 15, '2018-07-11'),
(22, 23, 18, '2018-07-11'),
(23, 23, 19, '2018-07-11'),
(24, 23, 28, '2018-07-11'),
(25, 23, 76, '2018-07-11'),
(26, 23, 50, '2018-07-11'),
(27, 23, 48, '2018-07-11'),
(28, 23, 78, '2018-07-11'),
(29, 25, 22, '2018-07-10'),
(30, 25, 15, '2018-07-10'),
(31, 25, 18, '2018-07-10'),
(32, 25, 19, '2018-07-10'),
(33, 25, 28, '2018-07-10'),
(34, 25, 76, '2018-07-10'),
(35, 25, 50, '2018-07-10'),
(36, 25, 48, '2018-07-10'),
(37, 25, 78, '2018-07-10'),
(38, 25, 22, '2018-12-05'),
(39, 25, 15, '2018-12-05'),
(40, 25, 18, '2018-12-05'),
(41, 25, 19, '2018-12-05'),
(42, 25, 28, '2018-12-05'),
(43, 25, 76, '2018-12-05'),
(44, 25, 50, '2018-12-05'),
(45, 25, 48, '2018-12-05'),
(46, 25, 78, '2018-12-05'),
(47, 26, 22, '2018-12-12'),
(48, 26, 15, '2018-12-12'),
(49, 26, 18, '2018-12-12'),
(50, 26, 19, '2018-12-12'),
(51, 26, 28, '2018-12-12'),
(52, 26, 76, '2018-12-12'),
(53, 26, 50, '2018-12-12'),
(54, 26, 48, '2018-12-12'),
(55, 26, 78, '2018-12-12'),
(56, 27, 22, '2018-12-05'),
(57, 27, 15, '2018-12-05'),
(58, 27, 18, '2018-12-05'),
(59, 27, 19, '2018-12-05'),
(60, 27, 28, '2018-12-05'),
(61, 27, 76, '2018-12-05'),
(62, 27, 50, '2018-12-05'),
(63, 27, 48, '2018-12-05'),
(64, 27, 78, '2018-12-05'),
(65, 28, 22, '2018-12-15'),
(66, 28, 15, '2018-12-15'),
(67, 28, 18, '2018-12-15'),
(68, 28, 19, '2018-12-15'),
(69, 28, 28, '2018-12-15'),
(70, 28, 76, '2018-12-15'),
(71, 28, 50, '2018-12-15'),
(72, 28, 48, '2018-12-15'),
(73, 28, 78, '2018-12-15'),
(74, 29, 22, '2018-12-15'),
(75, 29, 15, '2018-12-15'),
(76, 29, 18, '2018-12-15'),
(77, 29, 19, '2018-12-15'),
(78, 29, 28, '2018-12-15'),
(79, 29, 76, '2018-12-15'),
(80, 29, 50, '2018-12-15'),
(81, 29, 48, '2018-12-15'),
(82, 29, 78, '2018-12-15');

-- --------------------------------------------------------

--
-- Table structure for table `tb_transaksi_paket_timeline`
--

CREATE TABLE `tb_transaksi_paket_timeline` (
  `id` int(11) NOT NULL,
  `id_transaksi_paket` int(11) DEFAULT NULL,
  `id_item` int(11) DEFAULT NULL,
  `jenis` enum('Pesan','Ambil Pesanan') DEFAULT NULL,
  `tgl_pemesanan` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_transaksi_paket_timeline`
--

INSERT INTO `tb_transaksi_paket_timeline` (`id`, `id_transaksi_paket`, `id_item`, `jenis`, `tgl_pemesanan`) VALUES
(20, 14, 21, 'Pesan', '2018-09-29'),
(21, 14, 15, 'Pesan', '2018-09-29'),
(22, 14, 27, 'Pesan', '2018-09-29'),
(23, 14, 75, 'Pesan', '2018-09-23'),
(24, 14, 75, 'Ambil Pesanan', '2018-09-29'),
(25, 14, 77, 'Pesan', '2018-09-28'),
(26, 14, 77, 'Ambil Pesanan', '2018-09-29'),
(27, 14, 76, 'Pesan', '2018-09-23'),
(28, 14, 76, 'Ambil Pesanan', '2018-09-29'),
(29, 15, 21, 'Pesan', '2018-10-30'),
(30, 15, 15, 'Pesan', '2018-10-30'),
(31, 15, 27, 'Pesan', '2018-10-30'),
(32, 15, 75, 'Pesan', '2018-10-24'),
(33, 15, 75, 'Ambil Pesanan', '2018-10-30'),
(34, 15, 77, 'Pesan', '2018-10-29'),
(35, 15, 77, 'Ambil Pesanan', '2018-10-30'),
(36, 15, 76, 'Pesan', '2018-10-24'),
(37, 15, 76, 'Ambil Pesanan', '2018-10-30'),
(38, 16, 1, 'Pesan', '2018-08-29'),
(39, 16, 21, 'Pesan', '2018-08-29'),
(48, 21, 15, 'Pesan', '2018-12-28'),
(49, 21, 27, 'Pesan', '2018-12-28'),
(50, 21, 75, 'Pesan', '2018-12-22'),
(51, 21, 75, 'Ambil Pesanan', '2018-12-28'),
(52, 21, 77, 'Pesan', '2018-12-27'),
(53, 21, 77, 'Ambil Pesanan', '2018-12-28'),
(54, 21, 76, 'Pesan', '2018-12-22'),
(55, 21, 76, 'Ambil Pesanan', '2018-12-28'),
(56, 22, 1, 'Pesan', '2018-07-09'),
(57, 22, 21, 'Pesan', '2018-07-09'),
(58, 23, 22, 'Pesan', '2018-07-10'),
(59, 23, 15, 'Pesan', '2018-07-10'),
(60, 23, 18, 'Pesan', '2018-07-10'),
(61, 23, 19, 'Pesan', '2018-07-10'),
(62, 23, 28, 'Pesan', '2018-07-10'),
(63, 23, 76, 'Pesan', '2018-07-04'),
(64, 23, 76, 'Ambil Pesanan', '2018-07-10'),
(65, 23, 50, 'Pesan', '2018-07-10'),
(66, 23, 48, 'Pesan', '2018-07-10'),
(67, 23, 78, 'Pesan', '2018-07-04'),
(68, 23, 78, 'Ambil Pesanan', '2018-07-10'),
(69, 25, 22, 'Pesan', '2018-07-09'),
(70, 25, 15, 'Pesan', '2018-07-09'),
(71, 25, 18, 'Pesan', '2018-07-09'),
(72, 25, 19, 'Pesan', '2018-07-09'),
(73, 25, 28, 'Pesan', '2018-07-09'),
(74, 25, 76, 'Pesan', '2018-07-03'),
(75, 25, 76, 'Ambil Pesanan', '2018-07-09'),
(76, 25, 50, 'Pesan', '2018-07-09'),
(77, 25, 48, 'Pesan', '2018-07-09'),
(78, 25, 78, 'Pesan', '2018-07-03'),
(79, 25, 78, 'Ambil Pesanan', '2018-07-09');

-- --------------------------------------------------------

--
-- Table structure for table `tb_upacara`
--

CREATE TABLE `tb_upacara` (
  `id_upacara` int(11) NOT NULL,
  `nama_upacara` varchar(32) NOT NULL,
  `harga_upacara` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_upacara`
--

INSERT INTO `tb_upacara` (`id_upacara`, `nama_upacara`, `harga_upacara`) VALUES
(5, 'Upacara Bayi Tiga Bulan', ''),
(6, 'Upacara Melaspas Rumah', ''),
(7, 'Upacara Pagedong - gedongan', ''),
(8, 'Upacara Metatah', ''),
(9, 'Upacara Pawiwahan', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(255) NOT NULL,
  `username` varchar(32) NOT NULL,
  `nama` varchar(64) NOT NULL,
  `id_role` varchar(16) NOT NULL,
  `id_jenis_item` int(5) DEFAULT NULL,
  `password` varchar(32) NOT NULL,
  `deleted` int(2) DEFAULT '0',
  `no_hp` varchar(15) NOT NULL,
  `alamat` text NOT NULL,
  `email` varchar(100) NOT NULL,
  `kota` varchar(100) NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `username`, `nama`, `id_role`, `id_jenis_item`, `password`, `deleted`, `no_hp`, `alamat`, `email`, `kota`, `foto`) VALUES
(1, 'admin', 'Ice Frog', 'admin', 0, '827ccb0eea8a706c4c34a16891f84e7b', 0, '', '', '', '', 'profil.jpg'),
(2, 'dekorasi', 'PURNAMA DEKORASI', 'supplier', 7, '827ccb0eea8a706c4c34a16891f84e7b', 0, '', '', '', '', 'profil.jpg'),
(3, 'depot', 'DEPOT RAMA CATERING', 'supplier', 8, '202cb962ac59075b964b07152d234b70', 0, '', '', '', '', 'depot_1541510643_bawang-merah-manfaattanaman.jpg'),
(4, 'gusadhi', 'GUSADHI SOUNDSYSTEM', 'supplier', 10, '202cb962ac59075b964b07152d234b70', 0, '087654', 'denpasar barat', 'gusadhi@gmail.com', 'denpasar', 'gusadhi_1541510720_DaemonToolsUltra.png'),
(5, 'juwita', 'JUWITA SALON', 'supplier', 12, '202cb962ac59075b964b07152d234b70', 0, '123', 'tes alamat', '', '', 'profil.jpg'),
(6, 'banten', 'JRO MANGKU ALIT BANTEN', 'supplier', 9, '827ccb0eea8a706c4c34a16891f84e7b', 0, '', '', '', '', 'profil.jpg'),
(7, 'tabuh', 'SEKAA TABUH WAHYUDI GYR', 'supplier', 11, '202cb962ac59075b964b07152d234b70', 0, '', '', '', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_item`
--
ALTER TABLE `tb_item`
  ADD PRIMARY KEY (`id_item`);

--
-- Indexes for table `tb_jenis_item`
--
ALTER TABLE `tb_jenis_item`
  ADD PRIMARY KEY (`id_jenis_item`),
  ADD KEY `id_jenis_item` (`id_jenis_item`);

--
-- Indexes for table `tb_konfirmasi`
--
ALTER TABLE `tb_konfirmasi`
  ADD PRIMARY KEY (`id_konfirmasi`);

--
-- Indexes for table `tb_member`
--
ALTER TABLE `tb_member`
  ADD PRIMARY KEY (`id_member`);

--
-- Indexes for table `tb_notif`
--
ALTER TABLE `tb_notif`
  ADD PRIMARY KEY (`id_notif`);

--
-- Indexes for table `tb_paket`
--
ALTER TABLE `tb_paket`
  ADD PRIMARY KEY (`id_paket`);

--
-- Indexes for table `tb_paket_item`
--
ALTER TABLE `tb_paket_item`
  ADD PRIMARY KEY (`id_paket_item`);

--
-- Indexes for table `tb_pembayaran`
--
ALTER TABLE `tb_pembayaran`
  ADD PRIMARY KEY (`id_pembayaran`);

--
-- Indexes for table `tb_role`
--
ALTER TABLE `tb_role`
  ADD PRIMARY KEY (`id_role`);

--
-- Indexes for table `tb_supplier`
--
ALTER TABLE `tb_supplier`
  ADD PRIMARY KEY (`id_type_supplier`);

--
-- Indexes for table `tb_transaksi`
--
ALTER TABLE `tb_transaksi`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- Indexes for table `tb_transaksi_detail`
--
ALTER TABLE `tb_transaksi_detail`
  ADD PRIMARY KEY (`id_transaksi_detail`);

--
-- Indexes for table `tb_transaksi_paket`
--
ALTER TABLE `tb_transaksi_paket`
  ADD PRIMARY KEY (`id_transaksi_paket`);

--
-- Indexes for table `tb_transaksi_paket_detail`
--
ALTER TABLE `tb_transaksi_paket_detail`
  ADD PRIMARY KEY (`id_transaksi_paket_detail`);

--
-- Indexes for table `tb_transaksi_paket_item`
--
ALTER TABLE `tb_transaksi_paket_item`
  ADD PRIMARY KEY (`id_transaksi_paket_item`);

--
-- Indexes for table `tb_transaksi_paket_timeline`
--
ALTER TABLE `tb_transaksi_paket_timeline`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_upacara`
--
ALTER TABLE `tb_upacara`
  ADD PRIMARY KEY (`id_upacara`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_item`
--
ALTER TABLE `tb_item`
  MODIFY `id_item` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT for table `tb_jenis_item`
--
ALTER TABLE `tb_jenis_item`
  MODIFY `id_jenis_item` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tb_konfirmasi`
--
ALTER TABLE `tb_konfirmasi`
  MODIFY `id_konfirmasi` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_member`
--
ALTER TABLE `tb_member`
  MODIFY `id_member` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_notif`
--
ALTER TABLE `tb_notif`
  MODIFY `id_notif` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tb_paket`
--
ALTER TABLE `tb_paket`
  MODIFY `id_paket` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tb_paket_item`
--
ALTER TABLE `tb_paket_item`
  MODIFY `id_paket_item` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `tb_pembayaran`
--
ALTER TABLE `tb_pembayaran`
  MODIFY `id_pembayaran` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `tb_transaksi`
--
ALTER TABLE `tb_transaksi`
  MODIFY `id_transaksi` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tb_transaksi_detail`
--
ALTER TABLE `tb_transaksi_detail`
  MODIFY `id_transaksi_detail` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tb_transaksi_paket`
--
ALTER TABLE `tb_transaksi_paket`
  MODIFY `id_transaksi_paket` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `tb_transaksi_paket_detail`
--
ALTER TABLE `tb_transaksi_paket_detail`
  MODIFY `id_transaksi_paket_detail` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `tb_transaksi_paket_item`
--
ALTER TABLE `tb_transaksi_paket_item`
  MODIFY `id_transaksi_paket_item` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT for table `tb_transaksi_paket_timeline`
--
ALTER TABLE `tb_transaksi_paket_timeline`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT for table `tb_upacara`
--
ALTER TABLE `tb_upacara`
  MODIFY `id_upacara` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
